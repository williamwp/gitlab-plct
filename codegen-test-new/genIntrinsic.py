#!/usr/bin/env python3
from abc import ABC, abstractmethod
from enum import Enum
import sys

class AST(ABC):
    """Interface for the abstration syntax tree"""

    @abstractmethod
    def emit_assembly(self):
        """Emit the assembly content for this abstraction syntax tree"""
    
    @abstractmethod
    def emit_IR(self):
        """Emit the IR content for this abstraction syntax tree"""
    

class Intrinsic(AST):
    """ 
    This class is the AST of the intrinsic function.
    """

    def __init__(self, return_ty, function_name, argument_list):
        """
        Arguments:
            return_ty(vector type): the function return type
            function_name(str): just function name
            argument_list(list): list of vector type
        """
        self.return_ty = return_ty
        self.function_name = function_name
        self.argument_list = argument_list

    def emit_declare(self):
        """
        Emit the declare of the intrinsic function
        """
        declare = ''
        declare += 'declare ' + self.return_ty.emit_IR() + ' ' 
        declare += self.function_name.emit_IR()
        index = self.has_pointer()
        if index is not None:
            declare += '.' + 'p0' + Type.TYPE_TO_IR_TYPE[self.argument_list[index].type][:-1]
        declare += '('
        # fixme to eliminate the DRY
        for arg in self.argument_list:
            if arg is not self.argument_list[0]:
                declare += ', '
            declare += arg.emit_IR()
        if self.function_name.is_vsetvl():
            declare += ', ' + self.function_name.add_vtype(declare=True)
        declare += ');'
        return declare

    def has_pointer(self):
        for index in range(len(self.argument_list)):
            if self.argument_list[index].is_pointer():
                return index
        return None

    def emit_IR(self):
        IR = '%a =  ' if not self.return_ty.is_void() else ''
        IR += 'tail call '
        IR += self.return_ty.emit_IR() + ' ' + self.function_name.emit_IR()
        index = self.has_pointer()
        if index is not None:
            IR += '.' + 'p0' + Type.TYPE_TO_IR_TYPE[self.argument_list[index].type][:-1]
        IR += '('
        arg_num = 0
        # fixme to eliminate the DRY
        for arg in self.argument_list:
            if arg is not self.argument_list[0]:
                IR += ', '
            IR += arg.emit_IR()
            IR += ' %' + str(arg_num)
            arg_num += 1
        if self.function_name.is_vsetvl():
            IR += ', ' + self.function_name.add_vtype()
        IR += ')\n'
        IR += ('ret ' + self.return_ty.emit_IR() + ' %a') if not self.return_ty.is_void() else 'ret void'
        return IR

    def emit_assembly(self):
        if self.function_name.is_vsetvl():
            if self.function_name.add_vtype() == 'i64 0':
                return "vsetvl a1, a0, zero"
            return "vsetvl a1, a0, a1"
        assembly = ''
        assembly += self.function_name.emit_assembly()
        assembly += ' '
        assembly += self.return_ty.emit_assembly() + ', ' if self.return_ty.emit_assembly() else ''
        # fixme to eliminate the DRY
        for arg in self.argument_list:
            assembly += arg.emit_assembly()
            assembly += ', ' if self.argument_list[-1] != arg else ''
        return assembly

    def emit_define(self):
        define = 'define ' + self.return_ty.emit_IR() + ' '
        define += '@' + self.function_name.get_name() + '('
        arg_num = 0
        # fixme to eliminate the DRY
        for arg in self.argument_list:
            if arg is not self.argument_list[0]:
                define += ', '
            define += arg.emit_IR()
            define += ' %' + str(arg_num)
            arg_num += 1
        define += ')'
        return define

    def emit_total(self):
        total = self.emit_declare() + '\n'
        total += self.emit_define() + ' {\n'
        total += 'entry:\n'
        total += '; CHECK-LABEL: ' + self.function_name.get_name() + '\n'
        total += '; CHECK: ' + self.emit_assembly() + '\n'
        total += '; CHECK: ret \n'
        total += self.emit_IR() + '\n' 
        total += '}'
        return total

class FunName(AST):
    """ 
    This class represent the function name
    """
    VTYPE = {'e8':0, 'e16':4, 'e32':8, 'e64':12, 'm1':0, 'm2':1, 'm4':2, 'm8':3}

    def __init__(self, name):
        self.name = name

    def is_vsetvl(self):
        assert(len(self.name) >= 6)
        return True if self.name[:6] == 'vsetvl' else False

    def get_name(self):
        return self.name
    
    def emit_IR(self):
        split_name = self.name.split('_')
        split_name = split_name[:-1] if self.is_vsetvl() else split_name
        return '@llvm.riscv.' + '.'.join(split_name)

    def emit_assembly(self):
        split_name = self.name.split('_')
        return '.'.join(split_name[:-1])

    def add_vtype(self, declare=False):
        assert(self.is_vsetvl())
        if declare:
            return 'i64'
        vtype = self.name.split('_')[-1]
        assert('m' in vtype and 'e' in vtype)
        type_fields = vtype.split('m')
        return 'i64 ' + str(FunName.VTYPE[type_fields[0]] | FunName.VTYPE['m' + type_fields[-1]])

def get_all_type():
    """
    this function is use to generate a map that map 
    all of the type appear in intrinsic function
    """
    type_to_IR = {}
    for i in [8, 16, 32, 64]:
        for m in [1, 2, 4, 8]:
            type_to_IR['vint'+str(i)+'m'+str(m) + '_t'] = '<vscale x ' + str(m) + ' x i' + str(i) + '>'
            type_to_IR['vuint'+str(i)+'m'+str(m) + '_t'] = '<vscale x ' + str(m) + ' x i' + str(i) + '>'
    
    for m in [1, 2, 4, 8]:
        type_to_IR['vfloat16'+'m'+str(m) + '_t'] = '<vscale x ' + str(m) + ' x half>'
        type_to_IR['vfloat32'+'m'+str(m) + '_t'] = '<vscale x ' + str(m) + ' x float>'
        type_to_IR['vfloat64'+'m'+str(m) + '_t'] = '<vscale x ' + str(m) + ' x double>'

    for i in [8, 16, 32, 64]:
        type_to_IR['int'+str(i)+'_t*'] = 'i' + str(i) + '*'
        type_to_IR['uint'+str(i)+'_t*'] = 'i' + str(i) + '*'
    
    type_to_IR['float16_t*'] = 'half*'
    type_to_IR['float32_t*'] = 'float*'
    type_to_IR['float64_t*'] = 'double*'
    type_to_IR['void'] = 'void'
    type_to_IR['ptrdiff_t'] = 'i64'
    type_to_IR['size_t'] = 'i64'
    return type_to_IR

class Type(AST):
    """
    This class represent the type system
    """

    VR = ['v0', 'v1', 'v2', 'v3']
    GPR = ['a0', 'a1', 'a2']

    TYPE_TO_IR_TYPE = get_all_type()

    def __init__(self, type, index):
        self.index = index
        self.type = type

    def is_void(self):
        """
        return True if this type is void type
        """
        return True if self.type == 'void' else False

    def is_pointer(self):
        return True if self.type[-1] == '*' else False

    def is_vector(self):
        """
        return true if this type is vector type else false
        """
        return True if self.type[0] == 'v' and self.type != 'void' else False

    def emit_IR(self):
        return Type.TYPE_TO_IR_TYPE[self.type]

    def emit_assembly(self):
        if self.is_void():
            return ''
        memonic = Type.VR[self.index] if self.is_vector() else Type.GPR[self.index]

        return '0(' + memonic + ')' if self.is_pointer() else memonic

#global data of finite states
state2 = {chr(s):3 for s in range(48, 123)}
state2.update({'(':2, ')':2, '*':2, ',':2})
finite_states = [{}, state2, {}, {chr(s):3 for s in range(48, 123)}]
finalty = [1, 2, 3]

def tokenize(source):
    """
    input source(str), return tokens

    Arguments: source the test string
    return: list of tuple with (tokens, kind)
    """
    tokens = []
    current_state = 1
    starter_cussr = 0
    cussr = 0
    while cussr < len(source):
        assert(source[cussr] in finite_states[current_state])
        next_state = finite_states[current_state][source[cussr]]
        if next_state in finalty and ((cussr + 1 == len(source) or 
            source[cussr+1] == ' ') or source[cussr+1] not in 
            finite_states[next_state]):
            tokens.append(source[starter_cussr:cussr+1])
            current_state = 1
            step = 1 if (cussr + 1 < len(source) and source[cussr+1] != ' ') else 2
            starter_cussr = cussr + step
            cussr += step
            continue
        current_state = next_state
        cussr += 1
    return tokens

def parse(tokens):
    """
    a recursive desent parser

    the grammar is as follows:
    E --> T N ( A )
    T --> const TYPE *
    T --> TYPE
    A --> T A
    N --> NAME

    Argument: tokens, a list of tokens
    return: a Intrinsic abstract syntax tree
    """
    vector_operands_num = 0
    scalar_operands_num = 0

    def E(tokens_):
        return Intrinsic(T(tokens), N(tokens), A(tokens_))

    def N(tokens_):
        fun_name = FunName(tokens[0])
        del tokens_[0]
        return fun_name

    def T(tokens_):
        nonlocal vector_operands_num
        nonlocal scalar_operands_num
        if tokens_[0] == 'void':
            Ty = Type(tokens_[0], -1)
        elif tokens_[0][0] == 'v':
            Ty = Type(tokens_[0], vector_operands_num)
            vector_operands_num += 1
        else:
            if tokens_[0] == 'const':
                del tokens_[0]
            if len(tokens_) >= 1 and tokens_[1] == '*':
                Ty = Type(tokens_[0] + tokens_[1], scalar_operands_num)
                scalar_operands_num += 1
                del tokens_[0]
                del tokens_[1]
                return Ty
            scalar_operands_num += 1
            Ty = Type(tokens_[0], scalar_operands_num)
        del tokens_[0]
        return Ty

    def A(tokens_):
        assert(tokens_[0] == '(')
        del tokens_[0]
        arguments = []
        while tokens_[0] != ')':
            arguments.append(T(tokens_))
            # delete the argument name
            del tokens_[0]
            if tokens_[0] == ',':
                del tokens_[0]
        return arguments
    return E(tokens)
                
if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        for line in f:
            tokens = tokenize(line[:-1])
            intrinsic = parse(tokens)
            print(intrinsic.emit_total())
            print()