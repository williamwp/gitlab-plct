# auto generate intrinsic test code

## use Instruction:
* ``git clone``
* ``cd codegen-test``
* ``chmod +x ./genIntrinsic.py``

write the intrinsic function declare to a txt file, maybe called intrinsics.txt

* ``./genIntrinsic.py intrinsics.txt``

## for example:

write the following contents to intrinsics.txt

``` 
vint8m8_t vle8_v_i8m8 (const int8_t *base);
vint16m1_t vle16_v_i16m1 (const int16_t *base);
```

then run 

* ``./genIntrinsic.py intrinsics.txt``

the output is:

```
declare <vscale x 64 x i8> @llvm.riscv.vle8.v.i8m8(i8*);
define <vscale x 64 x i8> @vle8_v_i8m8(i8* %1) {
entry:
; CHECK-LABEL: vle8_v_i8m8
; CHECK: vle8.v v0, a0
; CHECK: ret 
%0 =  tail call <vscale x 64 x i8> @llvm.riscv.vle8.v.i8m8(i8* %1)
ret <vscale x 64 x i8> %0
}

declare <vscale x 4 x i16> @llvm.riscv.vle16.v.i16m1(i16*);
define <vscale x 4 x i16> @vle16_v_i16m1(i16* %1) {
entry:
; CHECK-LABEL: vle16_v_i16m1
; CHECK: vle16.v v0, a0
; CHECK: ret 
%0 =  tail call <vscale x 4 x i16> @llvm.riscv.vle16.v.i16m1(i16* %1)
ret <vscale x 4 x i16> %0
}
```




