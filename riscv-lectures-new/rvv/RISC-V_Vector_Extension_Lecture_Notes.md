# RISC-V 向量扩展 课程讲稿

## RISC-V指令集及其扩展简介（5分钟）

### RISC-V指令集简介

  RISC-V是2011年诞生的一个开源的指令集架构（ISA）。与几乎所有的旧架构不同，它属于一个开放的，非营利性质的基金会，未来不会受任何单一公司的决定的影响。RISC-V基金会的目标是保持RISC-V的稳定性，并力图让它如同Linux操作系统一样受欢迎。目前已经有很多的与RISC-V基金会合作的公司机构。
	
  RISC-V(“RISC five”)的目标是成为一个通用的指令集架构(ISA)
	* 它要能适应各种规模的处理器。（包括从最袖珍的嵌入式控制器，到最快的高性能计算机等）。
	* 它应该能兼容各种流行的软件栈和编程语言。
	* 它应该适应所有实现技术。（包括现场可编程门阵列(FPGA)、专用集成电路(ASIC)、全定制芯片，甚至未来的设备技术）
	* 它应该对所有微体系结构样式都有效。（例如微编码或硬连线控制;顺序或乱序执行流水线; 单发射或超标量等等）
	* 它应该支持广泛的专业化，成为定制加速器的基础，因为随着摩尔定律的消退，加速器的重要性日益提高。
	* 它应该是稳定的，基础的指令集架构不应该改变。更重要的是，它不能像以前的专有指令集架构一样被弃用，例如AMD Am29000、Digital Alpha、Digital VAX、 Hewlett Packard PA-RISC、Intel i860、Intel i960、Motorola 88000、以及Zilog Z8000。


### RISC-V的模块化特点与模块简介

  x86指令集自诞生以来指令数量的增长。x86在1978年诞生时有80条指令，到2015年增长了16倍， 到了1338条指令，并且仍在增长。2015年在英特尔的博客，有着 3600条指令的统计结果[Rodgers and Uhlig 2017]，这意味着x86指令的增长速率提高到了(在1978年到 2015年之内)每四天增长一条。这个增长的很大一部分是因为x86 ISA依赖于SIMD指令来实现数据级并行。

  计算机体系结构的传统方法是增量ISA，新处理器不仅必须实现新的ISA扩展，还必须实现过去的所有扩展。目的是为了保持向后的二进制兼容性，这样几十年前程序的二进制版本仍然可以在最新的处理器上正确运行。这一要求与来自于同时发布新指令和新处理器的营销上的诱惑共同导致了ISA的体量随时间大幅增长。例如，刚才图2显示了当今主导ISA 80x86的指令数量增长过程。这个指令集架构的历史可以追溯到1978年，在它的漫长生涯中，它平均每个月增加了大约三条指令。 这个传统意味着x86-32(我们用它表示32位地址版本的x86)的每个实现必须实现过去的扩展中的错误设计，即便它们不再有意义。例如，x86中的ASCII Adjust after Addition(aaa)指令，该指令早已失效，但仍需在后续设计中被添加。

   RISC-V的不同寻常之处，除了在于它是最近诞生的和开源的以外，还在于:和几乎所有以往的ISA不同，它是模块化的。RISC-V指令集的模块分为基础模块和扩展模块。RISC-V基础模块包括(RV32I, RV32E, RV64I, RV128I)。基础模块是固定的，指令集总是包含一个且仅一个基础模块，永远不会改变。这为编译器编写者，操作系统开发人员和汇编语言程序员提供了稳定的目标。RISC-V的扩展模块包括(MAFDCV等等)。扩展模块是可选的，分为标准扩展和用户自定义扩展，根据应用程序的需要，硬件可以包含或不包含这些扩展。这种模块化特性使得RISC-V具有了袖珍化、低能耗的特点，而这对于嵌入式应用可能至关重要。RISC-V编译器得知当前硬件包含哪些扩展后，便可以生成当前硬件条件下的最佳代码。习惯做法是把代表扩展的字母附加到指令集名称之后作为扩展指令集。例如，RV32IAMFD将加法(RV32A)乘法(RV32M)，单精度浮点(RV32F)和双精度浮点 (RV32D)的扩展添加到了基础指令集(RV32I)中。


## RISC-V向量扩展的设计背景（5-10分钟）

### RISC-V向量扩展简介

  顾名思义，RISC-V “V”扩展指的是将向量（Vector）操作的扩展添加到基础指令集中，向量扩展的规范官方仓库：https://github.com/riscv/riscv-v-spec/

  RISC-V向量扩展是RISC-V指令集的标准扩展模块之一。它主要为基础指令集添加了向量寄存器和各类向量指令，使得用户可以使用向量化来优化和加速程序代码。

### 为什么使用向量？

  (介绍数据并行)当存在大量数据可供应用程序同时计算时，我们称之为数据级并行性。数组是一个常见的例子。虽然它是科学应用的基础，但它也被多媒体程序使用。前者使用单精度和双精度浮点数据，后者通常使用8位和16位整数数据。最著名的数据级并行架构是单指令多数据(SIMD，Single Instruction Multiple Data)。 SIMD最初的流行是因为它将64位寄存器的数据分成许多个8位、16位或32位的部分， 然后并行地计算它们。操作码提供了数据宽度和操作类型。数据传输只用单个（宽）SIMD寄存器的load和store进行。把现有的64位寄存器进行拆分的做法由于其简单性而显得十分诱人。为了使SIMD更快，架构师随后加宽寄存器以同时计算更多部分。由于SIMD ISA属于增量设计阵营的 一员，并且操作码指定了数据宽度，因此扩展SIMD寄存器也就意味着要同时扩展SIMD指令集。将SIMD寄存器宽度和SIMD指令数量翻倍的后续演进步骤都让ISA走上了复杂度逐渐提升的道路，这一后果由处理器设计者、编译器编写者和汇编语言程序员共同承担。一个更老的，并且在我们看来更优雅的，利用数据级并行性的方案是向量架构。这里解释了我们在RISC-V中使用向量而不是SIMD的理由。

## RISC-V向量扩展的设计细节与特点（20-25分钟）

### RISC-V 向量扩展的设计细节与特点
1. #### New RISC-V “V” Vector Extension
目前，新的riscv向量扩展已经被添加作为riscv ISA的标准扩展，riscv向量扩展是一种更新的现代微处理器cray-style形式。今天介绍的v0.9版本已经非常接近向量扩展的最终版本，但是向量扩展的标准还在改进中，因此这份ppt中的一些细节可能会在之后被更改

2. #### RISC-V Implementation-defined Constant Parameters
每个支持向量扩展的 hart 都有三个参数的定义：
	1. 	单个向量元素的最大长度ELEN （以bit为单位，下同），要求 ELEN ≥ 8，并且必须为2的幂。
	2. 	向量寄存器的位数VLEN，要求VLEN ≥ ELEN，并且必须是2的幂。
	3. 	分段距离（The striping distance in bits）SLEN，要求必须为VLEN ≥ SLEN ≥ 32，并且必须为2的幂。

3. #### RISC-V Scalar State and vector additional state
向量scalar state有32个整数寄存器和32个浮点寄存器，每一个浮点寄存器都包含有单精度和双精度的浮点值，浮点状态寄存器被用来浮点rounding mode和异常报告。scalar state 针对不同的机器位长，对浮点数的体系结构支持，开发了不同的版本。
向量状态中有32个向量数据寄存器，每一个都是VLEN长度，VLEN可以对不同的向量体系结构设计进行定制，向量长度寄存器vl（单个向量中存放元素的个数), 向量类型寄存器vtype,其他的控制寄存器有vstart,用于tram handling一般情况下为0, vrm/vxsat 用于fixed-point rounding

4. #### Vector Type Register
向量类型寄存器中起作用的是前10位，其中最重要的是vlsew, vlmul[1:0], vlmul[2]字段，第一个表示每个元素的宽度，他的编码如右图所示，范围是从8-1024，vlmul用于表示向量group的长度，他的值由LMUL = 2vlmul = 1-8公式表示，vlmul[2]表示上面的lmul值是不是小数。这些变量在编译过程中可以被动态设置。即使在具体的硬件架构下，RVV中的向量组织形式也是可变的。

5. #### Example Vector Register Data Layouts (LMUL=1)
这个图说明了在lmul=1的情况下，在不同vlen,和不同sew的情况下, 向量寄存器的数据布局

6. #### Setting vector configuration, vsetvli/vsetvl
vsetvl{i}配置指令会设置vtype寄存器，并且也会设置vl寄存器，然后返回vl值到一个常量寄存器中，
vtype的值由vlen,sew,flmul,ta,ma字段编码组成，通常我们会用vsetvli来设置vtype参数，寄存器版本vsetvl通常被用来上下文的保存和恢复

7. #### vector load Instructions
这张图表示向量load指令的编码格式，向量laod指令有三种形式，分别是unit-stride,即一个元素对应一个元素这样laod，scalar stride,表示向量间隔laod，即空几个字节加载一个，向量indexed，指每个元素都有对应的索引。

8. #### Vector Store Instructions
这是向量存储指令，他有一个vs3寄存器来作为存储数据的地址。

9. #### vector load/store instructions
这是向量load/store的具体汇编表示

10. #### vector length multiplier LMUL
lmul用于group寄存器，可以获得更少但是更长的寄存器。他把lmul个寄存器当作一个寄存器来使用，lmul的值必须是2的幂次。他主要应用于处理较长向量的情况，这样可以减少指令的数量，有vlmul[1:0]和vlumul[2]确定

11. #### LMUL=8 stripmined vector memcpy example
这是一个unit-stripmined 的memcpy的例子，他先配置向量形式，然后load和store数据

12. #### vector integer add instructions
这里罗列了向量整数加法的指令

13. #### vector FP add instruction
这里罗列了向量浮点加法的指令

14. #### mask
1.几乎所有的操作都是可以在向量掩码下进行的，向量掩码一般都放在v0寄存器中
2.一个单个vmbit会在指令编码中被设置，用以表示这个指令是不是掩码形式的
3.更长的64位编码空间将会支持在任意的寄存器中predicate
4.整数和浮点的比较指令可以用来配置掩码到v0向量寄存器中
5.掩码可以执行逻辑运算

15. #### integer compare instructions
这是整数比较指令，用来配置掩码

16. #### mask logical operations
这是掩码的逻辑操作

17. #### widending Integer add instructions
这是widening 整数加法指令，它将两个对应元素运算后放入一个两倍宽的元素中，防止有overflow的情况


PLCT实验室的实习生陈影同学在知乎中文连载的《RISC-V "V"(向量)扩展规范v0.9+文档》详细介绍了RVV规范v0.9版本的具体内容，感兴趣的同学可以去学习一下。Link: [https://www.zhihu.com/people/cheng-le-yu-9/posts](https://www.zhihu.com/people/cheng-le-yu-9/posts)


### RISC-V向量扩展的特点以及与其他SIMD指令集的对比及其优势

1. #### RISC-V 向量扩展的特点

   1. Typed registers 

      1. 每个向量寄存器都有一个类型
      2. 不同向量寄存器可有不同的类型 
      3. 向量寄存器的类型可以通过 vcvt 指令动态调整 

   2. 变长向量

      RVV 中的向量长度不与架构绑定。RVV 指令集的具体实现可以自主选择提供多长的向量，而应用程序可以通过指令选择自己需要的向量长度。传统架构， 比如 x86, 从64位的 MMX 升级到128位的SSE, 再到256位的 AVX, 乃至最新的AVX-512， 长度的每一次提升都需要在指令集中加入大量新指令。而 RVV 则可自动适应不同长度的向量，这使得 RVV 指令集非常简洁。而且， 应用程序无需重新编译即可在不同实现上运行。

   3. 混合类型编程

      RVV 允在一条指令中使用不同长度的向量， 其中短向量会自动扩宽为长向量。 

      例如：

      ​	vadd v1<sub>i8</sub>, v2<sub>i8</sub> ->  v0<sub>i16</sub>， v1 将被提升为 i16 向量。

      ​	vadd v1<sub>i8</sub>, v2<sub>i64</sub> -> v<sub>i64</sub>， v1 将被提升为 i64 向量。


   4. 支持定点数和16位浮点数
   
   5. 可动态调整向量类型（SEW, LMUL）
          
   RVV可以在编译时动态调整向量类型参数以应对不同的数据类型和数据大小的输入。

   6. 高度可扩展

      RVV 指令可针对具体领域进行扩展， 比如矩阵计算、DSP、机器学习和图像处理。

   7. 对向量化的丰富支持

      1. 几乎每条指令都支持 Mask
      3. 各种访存模式（linear or strided loads and stores, scatters and gathers)
      4. 各种规约操作（sum, min/max, and/for...）
      5. 向量正交运算操作
      6. 通过数据依赖结束的fault-only-first loads循环指令 

2. #### 与其他 SIMD 指令集的对比及其优势

   1. Interl AVX-512

      1. 简介

         AVX 指令集是 x86 架构处理器中的指令集，被英特尔和 AMD 的处理器所支持。AVX 指令集由英特尔在2008年3月提出，并在2011年第一季度出品的 Sandy Bridge 系列处理器首获支持。

         AVX-512 是一组新的指令集，和之前的 SSE/AVX-128/AVX-256一样，都属于向量运算指令，支持更宽的数据。 

      2. 对比

		RVV 对比这些传统指令集架构的优势，一个是由于RISC-V的模块化特征带来的，没有历史包袱，不需要增量扩展从而添加大量的指令。另一个是由于其可变的硬件向量长度，不需要像AVX-512一样固定512位向量长度，可适用于更多的硬件实现。

   2. ARM SVE(Scalable Vector Extension)

      1. 变长向量, 具体实现可以选择支持128位至2048位的向量 
      2. 向量长度无关编程
      3. Per-lane predication 
         1. RVV 为了简化实现，选择编译器辅助预测， 而非硬件预测。
      4. Gathers and scatters 
      5. 定点数支持
      6. Horizontal and serialized vector operations.
      7. 651 instructions, 4000+intrinsics

      对比： SVE是比较新的指令集，最大亮点是变长向量，与RVV有异曲同工之妙。与之对比， RVV的主要优势是没有历史包袱，指令集简洁。


## RISC-V向量扩展intrinsics简介（5-10分钟）

Intrinsics一般是指高级编程语言中的低级汇编语言的接口。为了使C/C++这样的高级编程语言能够方便地使用RISC-V向量扩展的指令，还需要设计向量扩展相应的intrinsics。rvv官方intrinsics规范文档[https://github.com/riscv/rvv-intrinsic-doc](https://github.com/riscv/rvv-intrinsic-doc)

### 类型系统

不同的（SEW, LMUL）对，对应不同的向量数据类型。

不同长度的Mask对应不同的掩码类型。

### 通用命名规则

这份Intrinsics的API的目标是让C/C++可以访问所有的RISC-V "V"(向量)扩展的指令。所以这些Intrinsics的命名很接近汇编指令的助记符。

一般来说，Intrinsic命名将对返回类型进行编码。从名称中更容易知道Intrinsics的输出类型。此外，如果调用Intrinsics作为操作数（operand），那么命名中包含返回类型将更为直接。如果没有返回值，则Intrinsics将对输入值类型进行编码。

### 例外命名规则

如果Intrinsics在不同的输入类型下具有相同的返回类型，则不能直接对这些Intrinsics使用通用命名规则。对于不同的输入类型，它将导致相同的Intrinsics命名。例如存储操作没有返回值。而是使用存储值的类型来进行Intrinsics命名。比较指令的结果是掩码类型，且有多个不同的(SEW,MLUL)对映射到同一长度的掩码类型，因此需要对输入和输出类型都进行编码

### 在C/C++中如何使用

解读PPT代码。

PLCT实验室的在读研究生张尹同学在知乎发布的《RISC-V "V"(向量)扩展Intrinsics》详细介绍了RVV的intrinsics规范的具体内容，感兴趣的同学可以去学习一下。Link: [https://zhuanlan.zhihu.com/p/141363004](https://zhuanlan.zhihu.com/p/141363004)

## 向量扩展的支持现状（5-10分钟）

### GCC与LLVM的支持现状，介绍PLCT的仓库

目前，GCC工具链已经较完整支持了RISC-V "V"向量扩展的汇编，并且持续在跟进向量扩展的最新版本。目前尚未支持intrinsics。

GitHub Link:
binutils-gdb: [https://github.com/riscv/riscv-binutils-gdb/](https://github.com/riscv/riscv-binutils-gdb/)
gnu-toolchain: [https://github.com/riscv/riscv-gnu-toolchain/](https://github.com/riscv/riscv-gnu-toolchain/)

riscv的这两个gcc相关仓库中都有相应的rvv-0.7.x，rvv-0.8.x和rvv-0.9.x分支。对应支持了相应版本规范的RISC-V向量扩展。

PLCT实验室正在进行LLVM上的RISC-V向量扩展支持工作。

GitHub Link: [https://github.com/isrc-cas/rvv-llvm](https://github.com/isrc-cas/rvv-llvm)

在该仓库的主分支rvv-iscas分支中，目前已经完成了对RSIC-V向量扩展的最新版本v0.9版的汇编支持。

而在分支rvv-0.8中，实现了RSIC-V向量扩展v0.8版本的汇编支持和部分intrinsics的支持。

由于目前Intrinsics规范文档已经更新至最新的v0.9版本，后续intrinsics的完整支持也会在主分支上基于v0.9版本的汇编进行实现。

### GNU编译工具链针对向量扩展的使用注意事项

 在安装riscv-gnu-toochain时，如果想要使用RISC-V向量扩展，需要在clone下github仓库后切换至rvv分支（最新分支为rvv-0.9.x）。并且在configure时使用--with-arch选项添加v后缀（例如：--with-arch=rv32gcv）。


## 为LLVM支持RISC-V指令集扩展的简单指引（5-10分钟）

### 以PLCT为LLVM支持RISC-V向量扩展为例，简单介绍为LLVM支持RISC-V指令集扩展涉及到的代码

#### TableGen简介

TableGen是LLVM后端常常用于记录指令集，寄存器等信息的一种描述性语言。目标体系结构目录下的 .td 文件都是用TableGen语言来描述的。这些 .td 文件会经过tblgen工具批量生成C++源文件，它的好处就是我们描述的是大量信息的共同点，然后由工具批量生成，减少我们描述的工作量。使用tablegen语言也十分简单灵活。

#### 代码架构简介
 
  在rvv-llvm/llvm/lib/Target/RISCV目录下是设计向量扩展的主体代码。
 
  在rvv-llvm/llvm/test/MC/RISCV目录下是向量扩展的汇编测试文件和反汇编测试文件。
 
  在rvv-llvm/llvm/test/CodeGen/RISCV目录下是向量扩展的LLVM IR测试代码，其中包括最新实现的vsetvl.ll测试文件。
 
  下面根据rvv-llvm/llvm/lib/Target/RISCV目录介绍一下LLVM支持RISC-V向量扩展实现。

RISCV.td
    
  本文件中定义了向量扩展的FeatureStdExtV，与其他的imafdcb扩展一样，FeatureStdExtV继承了SubtargetFeature类，我们将向量扩展对应的名称进行定义。同时定义了HasStdExtV，我们可以使用本文件中定义的谓词HasStdExtV对向量扩展指令进行定义，RISCVInstrInfoV.td文件包含了相关的定义。

RISCVSubtarget.h

  本文件中定义了HasStdExtV函数，主要需要在RISCVSubtarget类中对HasStdExtV函数进行初始化，所有的RISCV Extension指令都是在这个类中进行初始化定义的。
  同时，我们需要在文件末尾对hasStdExtV()函数进行定义，描述它的返回类型，并且设置为const类型。const修饰函数返回值，它限定一个变量不允许被改变，产生静态作用。使用const在一定程度上可以提高程序的安全性和可靠性。

RISCVInstrFormats.td
  
  本文件中主要是对所有的RISCV扩展指令的格式进行描述，其中的 opcode names是在 RISC-V User-level ISA specification ("RISC-V base opcode map")中的Table 19.1定义的，RISCVOpcode是7位二进制数字。RISCVOpcode类对opcode进行了定义，后面的vector，amo和load/store操作的opcode编码都是来源于这个文件中的编码定义。

RISCVInstrFormatsV.td
  
  本文件中主要是对所有的RISCV向量扩展指令的格式（FormatsV）进行描述：
  
  RVVMaskCond类定义了RVV_Unmasked和RVV_Masked两种形式，Inst{28}=0b1表示没有进行掩码操作，Inst{25}=0b0表示已经进行了掩码操作。
  
  RVInstVLoad和RVInstVStore这两个类分别表示在LOAD-FP major opcode下的向量加载和存储类指令。
  
  RVInstVA类表示在OP-V major opcode下的向量运算指令。
  
  RVInstVSet类表示在OP-V major opcode下的向量设置指令。
  
  RVInstVAMO类表示在AMO major opcode下的向量原子操作指令

RISCVInstrInfoV.td
  
  本文件中主要是对所有的RISCV向量扩展指令进行定义，指令中类定义主要来自RISCVInstrFormatsV.td和本文件中的单独类定义。所以在本文件的最开始需要包括RISCVInstrFormatsV.td头文件。

RISCVRegisterInfo.td
  
  本文件中主要是对所有的RISCV向量扩展的向量寄存器v0-v31，同时还包括一些特殊寄存器，VR，VRV0，VL( Active vector length register)和VLR

RISCVISelLowering.cpp
  
  本文件中主要是对所有的RISCV向量扩展的vsetvl intrinsic函数定义，最开始在下面的函数中对 ISD::INTRINSIC_WO_CHAIN和LowerINTRINSIC_WO_CHAIN进行定义，以明确返回的参数类型和数量符合LowerOperation中的参数定义情况。

RISCVSystemOperands.td
  
  本文件中主要是对所有的RISCV向量扩展的6个用户级CSR寄存器和每个寄存器对应的地址。




## 参考文献

[Wikipedia: RISC-V](https://en.wikipedia.org/wiki/RISC-V#Vector_set)

[Adventures with RISC-V Vectors and LLVM](https://llvm.org/devmtg/2019-04/slides/TechTalk-Kruppe-Espasa-RISC-V_Vectors_and_LLVM.pdf)

[RISC-V VECTORS KNOW NO LIMITS](https://f-of-e.github.io/journal-club/722639bc9e641fbc2785d906043b7bd6.html)

[Intel® AVX-512 architecture evolutionand support in Clang/LLVM](https://llvm.org/devmtg/2014-10/Slides/Nis-AVX-512ArchPoster.pdf)

[Scalable Vector Extension (SVE) for Armv8-A](https://community.arm.com/developer/tools-software/hpc/b/hpc-blog/posts/technology-update-the-scalable-vector-extension-sve-for-the-armv8-a-architecture)