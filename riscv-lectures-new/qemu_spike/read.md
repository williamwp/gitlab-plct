# QEMU SPIKE 介绍和使用 课程讲稿

## Page 1
大家好，我是

## Page 2 目录
主要介绍 QEMU背景知识，QEMU基本原理

## Page 3 简介
QEMU是Fabrice Bellard开发的一款多架构支持的仿真器,目前最新版本是5.0.0。
QEMU官网介绍,“Qemu是一个广泛使用的开源计算机模拟器和虚拟机。”
当作为模拟器时，可以在一种架构（如x86 PC）下运行另一种架构（如ARM）下的操作系统和程序。通过使用动态转换，它可以获得非常好的性能。
作为虚拟机时，QEMU可以使用其他虚拟机管理程序（如 Xen 或 KVM）来使用CPU扩展（HVM）进行虚拟化，通过在主机CPU上直接执行客户机代码来获得接近于宿主机的性能。

这里简单而说，就是QEMU通过动态二进制翻译来模拟CPU，并提供一系列的硬件模型,使guest os和模拟的硬件交互，达到多平台guest os运行的效果。
关于Fabrice Bellard补充说明下，Fabrice Bellard开发出很多影响深远的软件作品，FFMPEG,QuickJS,TCC等,对仿真器感兴趣的同学还可以研究下
他的TinyEMU 也就是原来的 RISCVEMU，是一款小巧的运行在x86上128bit risc-v仿真器。

## Page 4 基本架构
QEMU在平台上的架构主要分为五个层次
最底层也就是我们的真实硬件平台一般为x86架构，或者Arm架构
第二层就是我们的宿主系统，可以是linux也可以是window乃至安卓
第三层就是我们QEMU本身，即运行的QEMU实例，在这个实例上
就是我们的第四层Guest OS,即程序的运行环境，Guest OS多种多样大到Win，Linux系(Fedora,Debian)这样的大型操作系统
也可能是RT-Thead,FreeRTOS小型MCU的IoT OS, 乃至只是带有一段引导的微系统
最后一层就是我们的App程序，即我们想要模拟的架构的程序。

我们来看下QEMU为了满足Guest OS的运行，提供了各式各样的模块模拟
QEMU模块主要包含CPU模拟，内存模拟，I/O设备模拟，总线模拟等，这里要说的是
由于是模拟，性能上肯定有很多制约，为此，qemu提供了KQEMU，KVM 这样的机制，用于实现
硬件虚拟化，达到加速的目的。

KVM是已经集成到Linux内核的一种加速机制，是X86架构且硬件支持虚拟化技术（IntelVT或AMD-V）的Linux的全虚拟化解决方案。它是Linux的一个很小的模块，利用Linux做大量的事，如任务调度、内存管理与硬件设备交互等。可以使用libvirt进行管理。
KQEMU也是QEMU的一种加速机制，不过需要Guest OS和Host OS都是X86时候才可以使用，我们这里就不做深入。

## Page 5 基本架构，架构支持
QEMU支持多种多样的CPU平台，如比较为人熟知的ARM,X86,MIPS,RISC-V等
其中，RISC-v近几年才新起，QEMU 2.9版本后功能逐步加入，要使用RISC-V的模拟，推进选用4.1.0及其之后的版本
这个是qemu官方wiki的一张平台支持表，这张表格中可以看到分为五列
Platform 就是QEMU主要的架构支持，也可以看到Alpha,PowerPC，s390x等比较小众的架构
Hardware Virtualization是硬件虚拟化支持，主要在ARM MIPS X86等平台
TCG Guest 是Guest os架构支持，全线支持，这也是QEMU的基本功能
MTTCG 是QEMU的新功能，用于TCG加速，关于TCG后面会简单介绍下
TCG Host 也就是 Host OS架构支持，目前ARM，x86，MIPS都做的不错

更多可以查看 参考链接，了解，当然我们也可以按照相关规则，添加模拟自己的架构
前提是了解qemu相关原理基础上，下面我们看下qemu的基本原理

## Page 6 基本原理
QEMU的实现主要建立在两块大的功能上
一个是TCG,也就是前面所说的二进制翻译
一个是QOM,也就是QEMU中CPU,总线,设备等模块实现的基础
下面我们了解下TCG

## Page 7-8 基本原理之TCG
TCG 也就是Tiny Code Generator,将源处理器机器代码转换为虚拟机运行所需的机器代码块(如x86机器代码块)
这张图来自Qemu Detailed Study，你一个印度学生对TCG原理的一个概览介绍，有兴趣的同学可以找了看看
翻页~~
TCG的工作流程主要分为两大过程：
guest code向TCG ops微码的转换，也就是qemu源码中gen_intermediate_code过程，如图所示，arm的guest os源码转换为 TCG ops微码，TCG ops微码也叫做TCG IR,因为QEMU的二机制翻译其实是从TCC演变而来的,guest os 源码的转换其实也是编译的过程。
还有一个过程是TCG ops微码向Host Code转换的过程，也就是qemu源码实现的tcg_gen_code

## Page 9 TCG IR
TCG IR是TCG ops微码的中间表示，Qemu源码中，对IR类别的七种定义:
分别是(读表格第一列): predefined ops 
选择性介绍指令示例
更多参加qemu wiki上内容，也就是这个链接
这里看到frontend-ops和backend-ops
frontend-ops和backend-ops基本是对应的
frontend-ops表现形式是tcg_gen_<op>[i]_<reg_size>(arguments),op 操作数，i是否含有立即数，reg_size是32/64
            比如: (a = b + c), 将 tcg_gen_xxx(a, b, c)
backend-ops 是frontend-ops的展开，可以理解为TCG IR的汇编形式

## Page 10 TCG IR 代码表示
TCG IR 代码表示是TCGOpcode，如图所示，也是前后端转换过程中的存储单元
name是指令的名称
oargs: output 参数个数
iargs: input 参数个数
cargs: const 参数个数
flags: 寄存器操作权限设置

## Page 11 QEMU QOM
qemu里一个重要的实现原理就是QOM, QOM全称QEMU Object Module，是用C语言实现的一种面向对象的编程模型。
看这两个小代码片段，如果是C++写一个类是如上图所示，而C没有类，要实现类的功能就转换为下图
为了解决
各种CPU架构和SOC的模拟和实现
模拟device与bus的关系

## Page 12 QEMU QOM 主体框架
Object(The base for all objects)  是所有的基类
ObjectClass(The base for all classes) 所有类基类
TypeInfo(工具)
TypeImpl
InterfaceInfo
InterfaceClass
以GPIO为例子

## Page 13 QEMU QOM 应用示例
点开一个，讲代码实现

## Page 14 QEMU基本使用
在对qemu的基本原理有所了解后，我们看看QEMU的基本使用
qemu的使用要知道，QEMU两方面的功能，一个是用户态仿真，一个是全系统仿真

## Page 15 用户态仿真
用户态仿真
QEMU可以在当前CPU上执行被编译为支持其他CPU的程序（例如：QEMU可以在x86机器上执行一个RISC-V二进制可执行程序）,简单的说就是程序加载到CPU运行

## Page 16 全系统仿真
在这种模式下，qemu完整的仿真目标平台，此时，qemu就相当于一台完整的pc机，例如包括一个或多个处理器以及各种外围设备

## Page 17 QEMU编译构建
第一步：获取源码
第二步：初始化仓库
第三步： 创建编译目录，config 源码
第四步：编译安装

## Page 18 编译生成介绍
qemu-ga：这是一个不利用网络实现 guest 和 host 之间交互的应用程序（使用 virtio-serial），运行在 guest 中。
qemu-io：这是一个执行 Qemu I/O 操作的命令行工具。
qemu-system-riscv64：Qemu 的核心应用程序，系统态虚拟机就由它创建的
qemu-riscv64：Qemu 的核心应用程序，用户态虚拟机就由它创建的
qemu-img：创建虚拟机镜像文件的工具
qemu-nbd：磁盘挂载工具

## Page 19 用户态运行
以hello world的为了，使用risc-v汇编写hello world程序

编译方法：

运行结果：

qemu-riscv32 -d的使用

## Page20 RISC-V to TCG IR
我们使用 -d in_asm查看 输入的asm,如图所示
看图说话

再使用-d op 查看优化前的TCG IR
看图说话

## Page21 RISC-V to TCG IR
看下add的转换流程

## Page22  TCG IR to Host 
我们使用-d out_asm查看最终host上的代码
看图说话

## Page23  TCG IR to Host 
转换流程主要经过TCG IR优化，变量生存周期优化，寄存器分配。。。

看完用户态使用，我们看看系统态的使用 
## Page 24-25 系统仿真
Fedora 是一个 Linux 发行版，是一款由全球社区爱好者构建的面向日常应用的快速、稳定、强大的操作系统。它允许任何人自由地使用、修改和重发布，无论现在还是将来。它由一个强大的社群开发，这个社群的成员以自己的不懈努力，提供并维护自由、开放源码的软件和开放的标准。

第一步：Download the Fedora disk images Decompress the disk image
第二步: Boot linux using RV64GC qemu

其中：
-nographic  
-machine virt 
-smp 4
-m 2G 
-kernel
-append
-device 

## Page 26 系统仿真
看过程

## Page 27 系统仿真(Debian)
自己实现

## Page 28 
接下来我们来介绍一下spike

## Page 29 总体概述  
相比于功能强大而庞大的qemu模拟器，spike是专门针对RISCV的轻量级模拟器，该模拟器的主体框架如下图所示。

Spike支持多核框架，每个核维护自身的状态信息（包括支持的指令集，指令信息列表，寄存器集合等）；
每个核都会有对应的一个MMU来进行内存访问，MMU会根据地址查询bus找到对应的内存块，然后根据块内偏移直接获取相应内存数据；
在这里，Spike虽然支持Cache机制，但Cache并不用于真实的内存访问，它更类似于一个跟踪和监控机制；而且不同于微架构中的私有L1 cache，在当前主分支Spike中是所有核都共享一个L1 cache，所以不存在cache一致性的问题；
除了这些以外，Spike还支持少量的设备模拟挂载在bus中，如Boot Rom(存储启动相关的代码), CLINT（用于产生计时器及软件中断）, Debug module（用于交互调试）。

## Page 30 spike使用
下面我们来看看spike的具体使用

## Page 31- 33 spike 基础命令

基于这个框架，spike能用于运行riscv系统或者利用pk运行riscv应用程序。spike也提供了一些选项用于对框架进行配置，或者辅助进行调试。接下来我们会对一些常见的选项进行简单的介绍。
spike的命令形式如下所示。
    spike [host options] <target program> [target options]
选项需要放在待运行RISCV程序之前
我对这些选项进行了初步的分类，包括一些基础选项：
帮助选项： -h，或者 --help， 这个是程序常见的命令，通过该命令，我们可以获取spike程序支持的选项以及相关描述；
配置选项：包括：
配置模拟的processor数目: -p<n>, 如p4，默认是1
配置模拟的指令集-isa=<name>, 默认是RV64IMAFDC， 可以指定为RV64IM或者RV64G(G代表IMAFD)，需要注意的是：这里的字母需要保证一定的顺序，如RVMI不是一个合法的输入，另外指令集间是包含依赖关系的，如不能单独支持双精度指令集D，而不支持单进度指令集；
除了核数以及指令集外，spike也支持起始pc的设置（--pc）以及内存的设置（-m<n>），并允许将内存分块，指定每块的起始地址和大小（-m<a:m,b:n,...>
除了基础配置，spike还支持一些扩展的配置：如
配置vector指令集支持的vector位宽的配置，默认为 vlen:128,elen:64,slen:128（单位为bit），这个配置需要遵顼一定规则，如vlen >= slen 和 elen， slen>=32, vlen <=4096
配置rocc扩展，目前spike自带的rocc扩展为dummy_rocc
配置icache，dcache，L2 cache的大小，包括set，way数目，以及block的字节数
配置共享库，共享库可用于扩展指令集的实现，也可以用于新版中plugin设备的实现
除了配置以外，spike也提供了一些调试选项，如
-d 用于开启交互的调试模式，通过spike自带的debug模块与用户进行交互，控制程序运行；
-rbb-port可以指定监听端口，配合openOCD（On Chip Debug），可以支持针对riscv程序的gdb调试，具体的调试过程在readme中有相关介绍（https://github.com/riscv/riscv-isa-sim/blob/f5bdc2e34299e3721c9319ba92dc72149f6af8a2/README.md）
另外，spike也提供了一些log选项来产生log信息，如指令执行信息（-l），cache miss信息（--log-cache-miss）或者dump出设备树信息（--dump-dts）

## Page 34 spike指令扩展
除了直接使用已有的spike运行调试自身实现的riscv程序以外，大家针对spike的工作是基于spike添加自身实现的指令，接下来将利用一个例子来介绍一下在spike中如何添加一条新指令，并介绍一下其中的内在原理。

## Page 35 spike指令添加
关于新建指令的流程，在spike代码的README.md上已经有了相关介绍，流程比较简单：
1）实现指令功能；2）添加指令编码；3）注册指令，将指令添加到processor的支持列表中；，最后重编译spike，为了测试我们添加的指令是否能正常运行，我们还需要编写测试案例来进行测试。
接下来，我们将利用这个博客（参考 https://quasilyte.dev/blog/post/riscv32-custom-instruction-and-its-simulation/#adding-mac-instruction-to-the-rv32im
）的mac指令例子来对各个步骤进行描述：

## Page 36-48 spike指令添加
首先添加指令功能的代码，这个过程实际上就是在riscv/insns目录下添加一个以指令名称命名的.h文件，这个是mac.h，这里会实现mac的功能代码:
它实现的功能的乘加功能，就是rd = rs1 * rs2 + rd；
这条指令是添加到M指令集的，所以通过require_extension判断当前模拟器是否支持M指令集，如果不支持使用该指令就会报出无效指令异常；
在指令的执行过程中用到很多宏，如RS1，RS2，READ_REG, WRITE_REG, 这些宏都是riscv/decode.h中提供的，以方便对指令操作数的提取，以及对寄存器的读写，类似的还有FRS1，FRS2等来获取浮点寄存器数据；除此之外，softfloat目录还提供了一些浮点运算的实现可以直接用于浮点指令的实现。大家在实现指令过程中可以参考其它riscv/insns目录下类似指令的实现来实现所需指令的功能。

大家可能也发现了，这个.h文件中实现的代码其实只是一个片段，那这个片段是怎么用的呢，这个片段会套进一个固定的指令模板中，代码在riscv/insn_template.cc中
这个模板针对每条指令提供两个接口：rv32_NAME和rv64_NAME的，里面的NAME和OPCODE会被指令名称，和指令的编码所替换。

这个替换过程中用到的指令编码就由第二步设置指令编码来产生。添加指令编码有两种方式：
比较方便的是利用riscv-opcode来生成
将指令编码添加到riscv-opcode的文件中，编码的格式包括指令的名称（mac），构成（包含rd，rs1，rs2），相应位的编码（31..25=1 14..12=0 6..2=0x1A 1..0=3）
利用这些信息，可以生成指令对应的match和mask编码信息（MATCH_MAC, MASK_MAC）以及指令的声明（DECLARE_INSN(MAC, MATCH_MAC, MASK_MAC)
）
另外一种方法是直接手动修改riscv/encoding.h，fesvr/encodin.h添加这些宏定义。

指令声明的过程其实是定义呢两个指令名相关的变量，它是为后续的指令注册服务的，指令注册由宏DEFINE_INSN来实现。
针对宏的利用也是通过代码生成的，这个过程就是把指令名称添加到riscv/riscv.mk.in文件的riscv_insn_list中
这个list会用于生成指令对应的DEFINE_INSN，添加到insn_list.h中
利用这个宏，会将指令的编码和功能接口描述注册到核的指令列表中，核在对指令解码时，会通过编码信息来匹配对应的描述，然后调用对应的接口来执行指令。
除了指令注册，list还用于根据指令功能模板生成对应的指令接口，实际上就是用指令名称和编码替换了模板中的NAME和OPCODE

在完成了这些过后，就可以对spike进行重编译。编译过程如下
cd "${RISCV}/riscv-isa-sim/build"
make
sudo make install
最后就是对指令实现进行测试：
为了避免对编译器进行修改，我们可以通过纯数字嵌入汇编的方式来实现我们所需测试的指令，如代码中的.word 0x02C5856B ，也就是我们希望实现的mac ra0 ra1 ra2。通常来说riscv的整型传参过程中用的就是ra0，ra1，ra2，所以博客中嵌入汇编中只有一行，但是这个会依赖于编译器优化，在我之前的测试过程中，当不添加优化时，参数会在函数头写入栈中，在用的时候再从栈中读出，并将结果协回到栈中，在纯数据的指令情况下，编译器会由于不识别指令，无法判断寄存器的使用情况，结果不会写回栈中，导致返回结果失败，所以保险起见，我们可以在嵌入汇编中添加一个搬移操作，来让编译器知道我们的总体寄存器使用情况。
为了验证我们结果的正确性，所以我们需要一种方法来获取我们的预期结果，并用测试结果与预期结果比较。
完成了测试案例实现，我们可以对测试案例进行编译和运行，并获取相应结果。

## Page 49
结束