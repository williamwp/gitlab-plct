# llvm-tag

**Compiling：**

Build with clang: If you don't have a custom front end, you can use the official LLVM Clang-9.0.0 version.
```
$ wget https://releases.llvm.org/9.0.0/cfe-9.0.0.src.tar.xz
$ tar xf cfe-9.0.0.src.tar.xz
$ mv cfe-9.0.0.src clang
$ git clone https://github.com/wangpeng_iie/llvm-tag.git
$ cd llvm-tag
$ mkdir build
$ cd build
$ cmake -DLLVM_TARGETS_TO_BUILD="RISCV" -DLLVM_ENABLE_PROJECTS=clang  -G "Unix Makefiles" ..
$ make -j $(nproc)
```

** Test Examples：**

```
$ ./bin/llvm-lit -v ../test/MC/RISCV/c910-valid.s
```

Result：

```
-- Testing: 1 tests, single process --
PASS: LLVM :: MC/RISCV/c910-valid.s (1 of 1)
Testing Time: 0.30s
  Expected Passes    : 1
```

