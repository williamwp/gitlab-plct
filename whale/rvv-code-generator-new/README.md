# 简介
这个项目是为 rvv 开发的代码生成器, 用于生成 riscv_vector.h, riscv_builtin.h 和 IntrinsicsRISCVVector.td 的内容.

整个项目使用 Common Lisp 编码. 运行代码需要 SBCL 版本 Lisp 解释器.

用户可以到 http://www.sbcl.org/platform-table.html 下载官方编译好的运行时. 

Ubuntu 用户可以执行 `apt-get install sbcl` 快速安装.

# 使用方法
```shell
sbcl --script riscv-emitter.lisp options...
```
可用选项包括

* --path-to-llvm-project \<path\>

    指定 llvm-project 根目录的路径. 生成各种文件时需要这个参数.
    
* --print-clang-intrinsics

    打印生成的 riscv_vecto.h 的内容.

* --print-clang-builtins
 
    打印生成的 riscv_builtin.h 的内容.

* --print-llvm-intrinsics

    打印生成的 IntrinsicsRISCVVector.td 的内容.
    
* --write-clang-intrinsics
  
    生成 riscv\_vector.h 并替换 llvm-project 源码树中的对应文件. 
    
    使用该选项需要指定 llvm-project 根目录的路径.

* --write-clang-builtins
  
    生成 riscv\_builtin.h 并替换 llvm-project 源码树中的对应文件.
    
    使用该选项需要指定 llvm-project 根目录的路径.

* --write-llvm-intrinsics
    
    生成 IntrinsicsRISCVVector.h 并替换 llvm-project 源码树中的对应文件.
    
    使用该选项需要指定 llvm-project 根目录的路径.

上述选项可以同时使用.

# 开发文档

## 设计思想
该项目采用 Common Lisp 实现了一个描述 Intrinsic 的嵌入式 DSL 以及批量生成函数声明的工具.
项目代码总体上是遵循面向对象思想来设计的. 

用户使用 Lisp 的原生语法结构以及项目提供的各种构造块描述 Intrinsic. Lisp 解释器处理这些描述信息时,
会批量生成 intrinsic 对象, 再进一步根据这些 intrinsic 对象生成各种函数声明.

## 源码结构

### 向量参数

* src/base-type.lisp

    该文件定义了 base-type 类, 用来表示标量或者向量元素的的大致类别. 
    
    目前 base-type 有三个实例，分别代表 “有符号整数”、“无符号整数”和“浮点数”。
    
    注意 base-type 不反映类型的宽度。base-type 需要与 std-element-width 结合起来才能表示一个 *具体的* 类型。
      
    base-type 类具有以下方法
    
    * c-type
        
        返回 C 语言中对应的大致类型, 比如 int, unsigned 或者 float. 
        
        这个方法用于生成各种 “typedef ...” 文本，比如 
        ```
            typedef __attribute__((riscv_vector(32, 1, 0)) int vint32m1_t;
        ```
        上例中的 “int” 就是代表有符号整数的 base-type 对象 c-type 方法返回的值。
    
    * single-letter
        
        返回类型缩写, 比如 i, u, 或者 f.
        
        single-letter 对应函数名后缀中的类型大类简写，比如函数名 vse32_v_i32m1 中的 "i"。
    
    * general-single-letter
        
        类似 single-letter, 但是当 single-letter 返回 u 时, general-single-letter 返回 i。
        
        这个方法用于生成 llvm 后端向量类型名中的类型大类简写，比如 llvm\_nxv1i32\_ty 中的"i"。
        
        因为 llvm 后端不区分有符号整数和无符号整数，所以对于无符号整数 general-single-letter 也返回 “i”。
        
* src/std-element-width.lisp

    该文件定义了 std-element-width 类, 对应于 rvv 文档中的SEW, 用来表示标量类型的宽度或者向量类型的宽度.
  
    std-element-width 类具有以下方法
    * sw-width 
    
        返回一个整数值, 代表类型宽度.
  
    * encoding
    
        返回一个整数值, 代表类型宽度的二进制编码, 用于 `#define _Exxx`
    
    * double-time
    
        返回一个宽度加倍的 std-element-width 对象.
  
    * definition
        
        返回 `#define _Exx` 字符串.

* src/length-multiplier.lisp

    该文件定义了 length-multiplier 类, 对应于 rvv 文档中的 LMUL.
  
    length-multiplier 类具有以下方法
    
    * lm-ratio
        
        返回一个分数, 表示 LMUL 值. 该值大于或者等于1时, 表示整数型 LMUL; 小于1表示分数型 LMUL.
    
    * is-fract
        
        判断该对象是否代表一个分数型 LMUL.
  
    * multiplier
    
    若该对象代表一个整数型 LMUL, 那么返回值就是 LMUL; 若代表一个分数型 LMUL, 那么返回值
    就是 LMUL 的分母。
  
    * encoding
    
        返回一个整数值， 用于 `#define _Lxxx`
  
    * double-time
    
        返回一个 ratio 翻倍的 length-multiplier 对象
  
    * lower-case
    
        返回一个字符串. 若该对象表示一个分数型 LMUL, 字符串为 "fxxx", 其中 xxx 为分母; 若表示一个整数型 LMUL，则返回 "xxx", xxx 为 LMUL 值。
  
    * upper-case
        
        与 lower-case 类似， 但是字符串中的 f 改为 F.
    
    * definition
        
        返回 "#define _Lxxx ..." 字符串.
    
### 基本类型

* src/element-type.lisp

    该文件定义了 element-type 类，用来表示向量的元素类型.
    该类具有如下方法
  
    * et-base-ty
        
        返回一个 base-type 对象，表示该类型的基本类型。
  
    * et-sew
    
        返回一个 std-element-width 对象， 表示该类型的宽度。
        注意 base-type 不带有宽度信. 它需要与 std-element-width 一起才能完整的反映一个类型。
  
    * c-type
    
        返回一个字符串，内容为 C 代码中的对应类型， 比如 int8\_t 和 uint64\_t.
  
    * unified
        
        与 c-type 类似，但是不带 _t 后缀.
    
    * abbr
    
        返回该类型的缩写，比如 Wi。规则同 Builtins.def。
  
    * unified-abbr
    
        返回该类型的缩写，比如 i8 和 u32。内容由 base-type 的 single-letter 与元素宽度拼接而来。
  
    * general-unified-abbr
    
        与 unified-abbr 类似，但是改为使用 general-single-letter。
  
    * llvm-type
    
        返回该类型对应的 llvm 后端类型， 比如 llvm\_i32\_ty。
  
    * double-time
    
        返回宽度翻倍的 element-type 对象。
    
* src/vector-type.lisp

    该文件定义了 vector-type 类，表示向量类型。
    该类具有如下方法
  
    * vt-elem-ty
        
        返回一个 element-type 对象，表示元素类型。
  
    * vt-lmul
    
        返回一个 length-multiplier 对象，表示该向量的 LMUL 值。
    
    * c-type
    
        返回一个字符串， 内容为该向量在 C 代码中对应的类型， 比如 vint32m1_t。
  
    * abbr
    
        返回该类型的缩写，比如 q1c。规则同 Builtins.def。
  
    * llvm-type
    
        返回该类型对应的 llvm 后端类型， 比如 llvm\_nxv1i32\_ty。
  
    * suffix
    
        返回类型的缩写， 比如 i32m1 和 f64m8。
    
    * double-time
    
        返回元素宽度和 LMUL 翻倍的 vector-type 对象。
  
    * corresponding-mask-type
    
        返回该向量类型对应的 mask 类型。各种 Intrinsic 的 masked 版本中的 mask 类型与其中的向量类型存在对应关系。
  
    * definition
    
        返回 "typedef \_\_attribute\_\_((riscv/_vector...." 字符串。
    
* src/mask-type.lisp

    该文件定义了 mask-type 类，表示 mask 类型。
    
    该类具有如下方法
  
    * c-type
    
        返回该类型在 C 代码中对应的类型， 比如 vbool8_t。
  
    * abbr
    
        返回该类型的缩写，比如 q8b。规则同 Builtins.def。
  
    * llvm-type
    
        返回该类型对应的 llvm 后端类型，比如 llvm\_nxv8i1\_ty。
  
    * unified-abbr
    
        返回该类型的缩写，比如 b8。
  
    * definition
        
        返回 "typedef \_\_attribute\_\_((riscv/_mask..." 字符串。
    
* src/tuple-type.lisp

    该文件定义了 tuple-type 类， 表示 tuple 类型。

    因为我不确定按照 Builtins.def 中的规则如何表示结构体，所以这个类目前还没有可用的方法。因此目前还不支持定义涉及 tuple 的 Intrinsics。
  
* src/scalar-specifier.lisp

    该文件定义了 scalar-specifier 类，表示带有修饰符号的标量类型， 比如 const int32_t *。
    
    该类具有如下方法
  
    * ssp-base
    
        返回一个字符串，表示该类的基本类型，比如 size\_t 和 ptrdiff\_t。
  
    * ssp-modifiers
    
        返回一个列表，包括该类具有的修饰符。
  
    * is-void
    
        判断该类是否是 void 类型。
    
    * is-overloaded
    
        判断该类型是否需要重载。size\_t，ptrdiff\_t 和各种指针类型都需要重载。
    
    * modified-by
    
        判断该类型是否具有某个修饰符。
    
    * c-type
    
        返回该类型在 C 代码中的对应类型， 比如 szie\_t。
  
    * abbr
    
        返回该类型的缩写，规则同 Builtins.def。
  
    * llvm-type
        
        返回该类型对应的 llvm 后端类型， 比如 llvm\_anyint\_ty。

### Intrinsic 描述对象

* src/intrinsic.lisp

    该类型定义了 intrinsic 类， 表示一个具体的 intrinsic。
    该类具有如下方法
  
    * intr-name
    
        返回一个字符串，表示函数的名字。
  
    * intr-return-ty
    
        返回该函数的返回值类型。
  
    * intr-params
    
        返回一个二元组的列表，表示该函数的参数列表。
  
    * intr-attrs
        
        返回一个字符串，表示该函数的属性， 比如 "n"。规则同 Builtins.def。
  
    * intr-props
    
        返回一个字符串的列表，表示该函数在 llvm 后端中的属性，比如 ["IntrNoMem"]。
    
    * intr-body
    
        返回该函数的函数体。当该值非空时， 表示这个函数不使用根据函数签名自动生成的函数体。
    
    * intr-gci
    
        判断该函数是否要在 riscv/_vector.h 中生成一个函数。
    
    * intr-gcb
    
        判断该函数是否要在 riscv/_builtin.h 中生成一个函数。
  
    * intr-gli
    
        判断该函数是否要在 IntrinsicsRISCVVector.td 中生成一个函数。
    
    * parameter-types
    
        返回一个 type-specifier 的列表， 表示该函数的参数类型列表。
  
    * parameter-names
    
        返回一个字符串的列表，表示该函数的参数名列表。
    
    * to-clang-intrinsic
    
        返回在 riscv/_vector.h 中自动生成的函数。
  
    * to-clang-builtin
    
        返回在 riscv/_builtin.h 中自动生成的函数。
  
    * to-llvm-intrinsic
        
        返回在 IntrinsicsRISCVVector.td 中自动生成的函数。
    
### 工具

* src/predefined-types.lisp

    该文件定义了 rvv 文档中涉及的各种常量、类型以及一些小工具。
  

* src/utils.lisp

    该文件定义了一些编码时用到的小工具.
    
    * gather
        
        这是一个宏，功能类似于 perl6 中的 gather/take 关键字。
        
        用户可以在 gather 包裹起来的代码中使用 take-all 函数。take-all 的所有参数会被收集到一个列表中，作为 gather 的返回值。
        
        另有一个 take 函数，功能类似与 take-all, 但是前者只接收一个列表，然后把列表中的元素全部传给 take-all。

        示例
        ```
            (gather 
                (take-all 1 2 3)
                (take-all 4 5 6))
        ```
        上例的 gather 调用会返回一个列表 '(1 2 3 4 5 6)。
        
    * interleave
        
        函数签名 `interleave (delimiter list)`。 
        
        这个函数在 `list` 的各个元素之间插入一个 `delimiter`。
        
        示例
        ```
            (interleave 0 '(1 2 3 4))
        ```
        上例的中 interleave 会返回一个列表 '(1 0 2 0 3 0 4)。
        
    * join
    
        函数签名 `join (items)`， 这里 `items` 是一个列表。
        
        join 把 `items` 中的对象转换成字符串之后按顺序拼接在一起。
        
    * combination

        函数签名 `combination (items)`, 这里 `items` 是一个列表。
        
        combination 的功能是枚举 items 中任意两个元素组成的无序二元组。
    
        示例 
        ```
            (combination '(1 2 3)')
        ```
        上例中的 combination 会返回一个列表 '((1 . 2) (1 . 3) (2 . 3))。
    
* src/interface.lisp

    该文件定义了编码时需要的一些接口.
  
# DSL 接口

* src/dsl.lisp

    该文件定义了 rvv-intrinsic 宏, 用来声明 Intrinic.
  
# Intrinsics 描述内容

* src/declarations.lisp

    该文件包含所有 Intrinsics 的声明.

### 程序入口

* riscv-emitter.lisp

    该文件定义了程序的入口.

# 添加新的 Intrinsics

因为添加新的 Intrinsics 需要编写 Lisp 代码并使用上面提到的各种常量和类型,
所以用户有 lisp 基础的话做起来会容易些.

这里 http://www.gigamonkeys.com/book/ 是一份开放的 Common Lisp 教程, 可供参考.

所有的描述信息放都在 src/declarations.lisp 中, 添加新信息时也放到这个文件里去.

描述 Intrinsic 用到的最基本的工具是 rvv-intrinsic. 其用法如下

```lisp
(rvv-intrinsic
  <return-type> <name> <parameters> 
  <attributes> <properties> <body>
  <gci> <gcb> <gli> <mask> <maskedoff>)
```
其中

* return-type

    返回值类型, 必填项. 可以填预定义的类型.

    预定义类型包括 void\_t, size\_t, ptrdiff\_t, 向量, 向量元素以及使用修饰符衍生而来的类型.

* name

    函数名, 必填项. 可以是一个字符串, 也可以是一个列表 `(格式化字符串 参数...)`.
  
    当此项为字符串时, 生成器直接把该字符串当作函数的名字；若是列表, 生成器则调用类似
    C 语言中的 printf 的函数, 根据格式化字符串以及参数动态生成函数名字. 格式化字符串使用`~a`
    作为统一的占位符. 生成器将把这些占位符替换为对应的参数.

* parameters

    参数列表, 必填项, 形式为一个二元组的列百. 元组的第一个元素为参数类型, 可取的值为所有预定义类型;
    第二个元素为参数名称, 形式为字符串.

* attributes

    前端属性, 选填项, 形式为字符串, 比如 "n" . 规则同 Builtins.def. 默认为空串. 

* properties

    后端属性, 选填项, 形式为字符串, 比如 "s". 默认为 "n".
  
    生成器会把这个字符串解析为具体的后端属性. 目前支持的属性有 
    * s -> IntrHasSideEffects
    * n -> IntrNoMem
    * r -> IntrReadMem
    * w -> IntrWriteMem.

* body

    函数体, 选填项, 形式为字符串. 若填入该项, 生成器在 riscv/_vector.h 中生成函数时, 将使用该函数体
    而非根据函数签名生成的函数体. 

* gci (generate clang intrinsics)

    是否在 riscv/_vector.h 中生成函数, 选填项, 取 t 或者 nil. 默认为 t.

* gcv (generate clang builtins)

    是否在 riscv/_builtin.h 中生成函数, 选填项, 取 t 或者 nil. 默认为 t.

* gli (generate llvm intrinsics)

    是否在 IntrinsicsRISCVVector.td 中生成函数, 选填项, 取 t 或者 nil. 默认为 t.

* mask

    该函数是否具有 masked 版本, 取值为 nil 或者向量类型. 默认为 nil.

* maskedoff

    该函数的 masked 版本是否具有 masked 参数, 取值为 nil 或者 t. 默认为 nil.
    
    该参数仅在 mask 参数不为 nil 时有意义.

必填项的之间的顺序是固定的, 不能调整；选填项必须放在必填项之后, 但是选填项之间的顺序无要求.

必填项输入时直接输入其值； 选填项输入时要在值之前加上引导符号, 形式为冒号接名称.

## 示例

例1:
```lisp
;; Set vl and vtype Functions
(rvv-intrinsic
 size_t "vsetvl" ((size_t "avl") (size_t "mode"))
 :properties "sn" :gci nil)
```
解释:

该函数返回值为 size\_t, 函数名称为 vsetvl. 

该函数有两个参数: 第一个类型为 size\_t, 名为 avl；第二个类型也为 size\_t, 名为 mode. 

该函数具有后端属性 IntrHasSideEffects 以及 IntrNoMem. 

生成器生成代码时, 不会在 riscv/_vector.h 生成这个函数.

上例指示生成器在 riscv_builtin.h 中生成如下代码

```
    RISCVBuiltin(vsetvl, "zzz", "", 0, 1, 2, )
```

其中 `RISCVBuiltin(...)` 是一个宏调用，表示这是一个 rvv 的 intrinsic, “vsetvl" 为其函数名;

"zzz" 依次表示 vsetvl 的返回值和参数类型，这里都是 size_t; 

第三个宏参数 “” 表示 vsetvl 的前端属性，这里为空表示没有前端属性;

后面的 “0, 1, 2, ”表示 vsetvl 的返回值类型和参数类型都是重载的(overloaded)。
CGBuiltin.cpp 中涉及 rvv 的代码把 intrinsic 的返回值类型和参数类型放到一个列表里，然后用这里的“0, 1, 2”作下标去检索
重载了的类型。

目前 size\_t、ptrdiff\_t 以及各种指针类型都是重载的。当函数的返回值或者参数中出现这些类型时，必须要在这里列出其下标。

上例还指示生成器在 IntrinsicsRISCVVector.td 中生成如下代码
```
def int_riscv_vsetvl
  : Intrinsic<[llvm_anyint_ty],
              [llvm_anyint_ty, llvm_anyint_ty],
              [IntrHasSideEffects,IntrNoMem]>;
```
其中 "int\_riscv\_vsetvl" 由固定前缀 “int\_riscv_” 与 函数名 “vsetvl”拼接而来，表示 vsetvl 在后端的函数名。

第二行的 llvm\_anyint\_ty 由 vsetvl 的返回值类型 size\_t 映射而来，表示 int\_riscv\_vsetvl 的返回值类型。

第三行的两个 llvm\_anyint\_ty 由 vsetvl 参数类型 size\_t 映射而来，表示 int\_riscv\_vsetvl 的参数类型。

第三行表示 int\_riscv\_vsetvl 的后端属性。这两个属性是由 `:properties "sn"`指定的。


例2:
```lisp
;; Vector Unit-Stride Load/Store Functions
(loop for vt in (enumerate-vector-types) do
  (let* ((et (vt-elem-ty vt))
          (ptr (ptr-to et))
          (cptr (const-ptr-to et))
          (w (sw-width (et-sew et))))
     (rvv-intrinsic
      vt ("vle~a_v_~a" w (suffix vt)) ((cptr "src"))
      :mask vt :maskedoff t)
     (rvv-intrinsic
      void_t ("vse~a_v_~a" w (suffix vt)) ((ptr "base") (vt "value"))
      :mask vt)))
```

解释:

上面的代码会批量生成 vlexxx\_v\_xxx 和 vsexxx\_v\_xxx 系列函数.

第2行的 `(enumerate-vector-types)` 返回一个列表,  包含全部预定义的向量类型.

loop 是 Common Lisp 原生的循环结构, 这里用来遍历向量列表并把类型放到 vt 中去.

第3行到第6行取出关于当前向量类型的各种信息, 其中 et 为向量元素的类型, ptr 是给 et 加上 `*` 修饰符后得到的指针类型, cptr 是给 et 加上 `const *` 得到的指针类型, w 是向量元素的宽度.

第7行到第9行描述 vlexxx\_v\_xxx 系列函数. 函数返回值类型为 vt.

这里函数名为 `("vle~a_v_~a" w (suffix vt))`. 生成器将会把第一个 ~a 替换为 w 的值, 把第二个 ~a 替换为 (suffix vt).

假设在当前循环下, vt 为 vint32m1\_t, 那么函数名将会是 vle32\_v\_i32m1.

该函数只有一个参数, 类型为指向 et 的不可修改指针, 名为 src.

该函数具有 masked 版本, 且 masked 版本中具有 maskedoff 参数.

后面的代码类似, 不再赘述.

上例指示生成器在 riscv\_vector.h 中生成一系列 vlexxx 和 vsexxx函数。这里取一个作简要解释。
```
static __attribute__((always_inline))
vint16m1_t vle16_v_i16m1(const int16_t* src) {
  return __builtin_riscv_vle16_v_i16m1(src);
}
```
上面的代码中，第一行是固定内容。

第二行为函数签名， 其中函数名 vle16_v_i16m1 是由`("vle~a_v_~a" w (suffix vt))` 指定的。

第三行为函数体。因为上例没有指定函数体，这里生成器直接生了调用 \_\_builtin\_riscv\_vle16\_v\_i16m1 的
代码。“\_\_builtin\_riscv\_vle16\_v\_i16m1” 这个名字由固定前缀 “\_\_builtin\_riscv\_” 与函数名 “vle16\_v\_i16m1”
拼接而来，对应前端的 intrinsic 名字。

上例还指示生成器在 riscv\_builtin.h 和 IntrisncsRISCVVector.td 中生成代码。相关解释与例1类似，这里不再赘述。

# 注意

Common Lisp 采用动态类型, 编译时没有可靠的类型检查. 并且这个项目在编码时也没有加入额外的类型检查代码. 所以如果编码时存在错误, 解释器会报一堆看起来很奇怪的错误信息. 建议写一段代码之后就运行一下代码看看是否正确.

解释器报错了也别慌, 认真阅读它给的错误信息, 按照提示找到错误源头就能修复问题:).

(我还没搞清楚怎么在我的电脑上切换全角/半角符号, 所以这篇文档里的符号一会儿全角一会儿半角的).
