(defgeneric encoding (value))

(defgeneric double-time (value))

(defgeneric constant-definition (value))


(defgeneric is-overloaded (specifier))

(defgeneric is-void (specifier))

(defgeneric c-type (specifier))

(defgeneric abbr (specifier))

(defgeneric llvm-type (specifier))

(defgeneric unified-abbr (specifier))

(defgeneric definition (object))


(defclass type-specifier nil nil)

(defmethod is-overloaded ((specifier type-specifier)) nil)

(defmethod is-void ((specifier type-specifier)) nil)
