(defclass element-type (type-specifier)
  ((base-type
    :initarg :b
    :reader et-base-ty)
   (sew
    :initarg :s
    :reader et-sew)))

(defmethod c-type ((specifier element-type))
  (with-slots (base-type sew) specifier
    (ecase (single-letter base-type)
      ((#\i) (ecase (sw-width sew)
               ((8) "int8_t")
               ((16) "int16_t")
               ((32) "int32_t")
               ((64) "int64_t")))
      ((#\u) (ecase (sw-width sew)
               ((8) "uint8_t")
               ((16) "uint16_t")
               ((32) "uint32_t")
               ((64) "uint64_t")))
      ((#\f) (ecase (sw-width sew)
               ((16) "_Float16")
               ((32) "float")
               ((64) "double"))))))

(defmethod unified ((specifier element-type))
  (with-slots (base-type sew) specifier
    (ecase (single-letter base-type)
      ((#\i) (ecase (sw-width sew)
               ((8) "int8")
               ((16) "int16")
               ((32) "int32")
               ((64) "int64")))
      ((#\u) (ecase (sw-width sew)
               ((8) "uint8")
               ((16) "uint16")
               ((32) "uint32")
               ((64) "uint64")))
      ((#\f) (ecase (sw-width sew)
               ((16) "float16")
               ((32) "float32")
               ((64) "float64"))))))

(defmethod abbr ((specifier element-type))
  (with-slots (base-type sew) specifier
    (ecase (single-letter base-type)
      ((#\i) (ecase (sw-width sew)
               ((8) "Sc")
               ((16) "Ss")
               ((32) "Zi")
               ((64) "Wi")))
      ((#\u) (ecase (sw-width sew)
               ((8) "Uc")
               ((16) "Us")
               ((32) "UZi")
               ((64) "UWi")))
      ((#\f) (ecase (sw-width sew)
               ((16) "h")
               ((32) "f")
               ((64) "d"))))))

(defmethod unified-abbr ((specifier element-type))
  (with-slots (base-type sew) specifier
    (join (list (single-letter base-type) (sw-width sew)))))

(defun general-unified-abbr (element-type)
  (with-slots (base-type sew) element-type
    (join (list (general-single-letter base-type) (sw-width sew)))))

(defmethod llvm-type ((specifier element-type))
  (with-slots (base-type sew) specifier
    (ecase (single-letter base-type)
      ((#\i) (ecase (sw-width sew)
               ((8) "llvm_i8_ty")
               ((16) "llvm_i16_ty")
               ((32) "llvm_i32_ty")
               ((64) "llvm_i64_ty")))
      ((#\u) (ecase (sw-width sew)
               ((8) "llvm_i8_ty")
               ((16) "llvm_i16_ty")
               ((32) "llvm_i32_ty")
               ((64) "llvm_i64_ty")))
      ((#\f) (ecase (sw-width sew)
               ((16) "llvm_half_ty")
               ((32) "llvm_float_ty")
               ((64) "llvm_double_ty"))))))

(defmethod double-time ((value element-type))
  (with-slots (base-type sew) value
    (with-slots (width) sew
      (let ((new-sew
             (make-instance 'std-element-width :w (* width 2))))
        (make-instance 'std-element-width
                       :b base-type
                       :s new-sew)))))
