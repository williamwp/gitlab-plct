(defclass scalar-specifier (type-specifier)
  ((base
    :initarg :b
    :reader ssp-base)
   (abbr
    :initarg :a
    :reader ssp-abbr)
   (modifiers
    :initarg :m
    :initform nil
    :reader ssp-modifiers)
   (overloaded
    :initarg :o
    :reader ssp-overloaded)
   (llvm
    :initarg :lv
    :reader ssp-llvm)))

(defmethod is-void ((specifier scalar-specifier))
  (string= "v" (ssp-abbr specifier)))

(defmethod is-overloaded ((specifier scalar-specifier))
  (slot-value specifier 'overloaded))

(defun modified-by (scalar-specifier modifier)
  (find modifier (ssp-modifiers scalar-specifier) :test #'string=))

(defmethod c-type ((specifier scalar-specifier))
  (with-slots (modifiers base) specifier
    (join
     (remove nil (list
                  (if (modified-by specifier "const") "const ")
                  (if (modified-by specifier "unsigned" ) "unsigned ")
                  base
                  (if (modified-by specifier "*") "*")
                  (if (modified-by specifier "&") "&"))))))

(defmethod abbr ((specifier scalar-specifier))
  (with-slots (prefixes postfixes abbr) specifier
    (join
     (remove nil (list
                  (if (modified-by specifier "unsigned") "U")
                  abbr
                  (if (modified-by specifier "const") "C")
                  (if (modified-by specifier "*") "*")
                  (if (modified-by specifier "&") "&"))))))

(defmethod llvm-type ((specifier scalar-specifier))
  (slot-value specifier 'llvm))
