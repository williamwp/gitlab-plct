(defclass tuple nil
  ((vector_type :initarg :vector-type)
   (size :initarg :size)))

(defmethod c-type ((specifier tuple))
