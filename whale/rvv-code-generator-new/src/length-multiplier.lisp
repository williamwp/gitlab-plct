(defclass length-multiplier nil
  ((ratio
    :initarg :r
    :reader lm-ratio)))

(defun is-fract (lmul)
  (< (slot-value lmul 'ratio ) 1))

(defun multiplier (lmul)
  (with-slots (ratio) lmul
    (if (is-fract lmul)
        (denominator ratio)
      (numerator ratio))))

(defmethod encoding ((lmul length-multiplier))
  (logior (ash (if (is-fract lmul) 1 0) 5)
          (round (log (multiplier lmul) 2))))

(defmethod double-time ((lmul length-multiplier))
  (with-slots (ratio) lmul
    (make-instance 'length-multiplie :r (* ratio 2))))

(defun lower-case (lmul)
  (format nil "~a~a" (if (is-fract lmul) "f" "") (multiplier lmul)))

(defun upper-case (lmul)
  (string-upcase (lower-case lmul)))

(defmethod definition ((object length-multiplier))
  (format nil "#define _M~a ~a" (upper-case object) (encoding object)))
