(defclass tuple-type (type-specifier)
  ((vector_type
    :initarg :v
    :reader tt-vector-ty)
   (size
    :initarg :s
    :reader tt-size)))

(defmethod c-type ((specifier tuple-type))
  (let ((vt (tt-vector-ty specifier))
        (s (tt-size specifier))
        (et (vt-elem-ty vt))
        (lmul (vt-lmul vt)))
    (format nil "v~am~ax~a_t" (unified et) (lower-case lmul) s)))
