;; intrinsics used in opencv

;; Set vl and vtype Functions
(rvv-intrinsic
 size_t "vsetvl" ((size_t "avl") (size_t "mode"))
 :properties "sn" :gci nil)

(rvv-intrinsic
 size_t "vsetvlmax" ((size_t "mode"))
 :properties "sn" :gci nil)

(loop for width in (mapcar #'sw-width (enumerate-sews)) do
  (loop for mul in (mapcar #'multiplier (enumerate-lmuls)) do
    (rvv-intrinsic
     size_t ("vsetvl_e~am~a" width mul) ((size_t "avl"))
     :body ("  return __builtin_riscv_vsetvl(avl, _E~a | _M~a);" width mul)
     :gcb nil :gli nil)
    (rvv-intrinsic
     size_t ("vsetvlmax_e~am~a" width mul) ()
     :body ("  return __builtin_riscv_vsetvlmax(_E~a | _M~a);" width mul)
     :gcb nil :gli nil)))

;; Vector Unit-Stride Load/Store Functions
(loop for vt in (enumerate-vector-types) do
  (let* ((et (vt-elem-ty vt))
         (ptr (ptr-to et))
         (cptr (const-ptr-to et))
         (w (sw-width (et-sew et))))
    (rvv-intrinsic
     vt ("vle~a_v_~a" w (suffix vt)) ((cptr "src"))
     :properties "r")
    (rvv-intrinsic
     void_t ("vse~a_v_~a" w (suffix vt)) ((ptr "base") (vt "value"))
     :body ("  __builtin_riscv_vse~a_v_~a(value, base);" w (suffix vt) )
     :gcb nil :gli nil)
    (rvv-intrinsic
     void_t ("vse~a_v_~a" w (suffix vt)) ((vt "value") (ptr "base"))
     :properties "w" :gci nil )))

;; Integer and Floating-Point Scalar Move Functions
(loop for vt in (enumerate-vector-types) do
  (let* ((et (vt-elem-ty vt))
         (bt (et-base-ty et))
         (ptr (ptr-to et))
         (cptr (const-ptr-to et))
         (w (sw-width (et-sew et)))
         (sf (suffix vt))
         (abbr (unified-abbr et)))
    (ecase (general-single-letter bt)
      ((#\i)
       (rvv-intrinsic
        et ("vmv_x_s_~a_~a" sf abbr) ((vt "src")))
       (rvv-intrinsic
        vt ("vmv_s_x_~a" sf) ((vt "dst") (et "src"))))
      ((#\f)
       (rvv-intrinsic
        et ("vfmv_f_s_~a_~a" sf abbr) ((vt "src")))
       (rvv-intrinsic
        vt ("vfmv_s_f_~a" sf) ((vt "dst") (et "src")))))))

;; Vector Initialization Functions
(loop for vt in (enumerate-vector-types) do
  (let ((sf (suffix vt)))
    (rvv-intrinsic
     vt ("vzero_~a" sf) ())
    (rvv-intrinsic
     vt ("vundefined_~a" sf) ())))

;; Vector Integer/Floating Move Functions
(loop for vt in (enumerate-vector-types) do
  (let ((et (vt-elem-ty vt))
        (sf (suffix vt)))
    (rvv-intrinsic
     vt ("vmv_v_v_~a" sf) ((vt "src")))
    (ecase (general-single-letter (et-base-ty et))
      ((#\i)
       (rvv-intrinsic
        vt ("vmv_v_x_~a" sf) ((et "src"))))
      ((#\f)
       (rvv-intrinsic
        vt ("vfmv_v_f_~a" sf) ((et "src")))))))

;; Reinterpret between different types
(defun rvv-reinterpret (src-vt dst-vt)
  (let* ((sf (suffix dst-vt))
        (src-et (vt-elem-ty src-vt))
        (dst-et (vt-elem-ty dst-vt))
        (sua (unified-abbr src-et))
        (dua (unified-abbr dst-et)))
    (rvv-intrinsic
     dst-vt ("vreinterpret_~a_~a_~a" dua sua sf) ((src-vt "src")))))

(flet ((hash-same-sew (vt)
         (let ((w (sw-width (et-sew (vt-elem-ty vt))))
               (r (lm-ratio (vt-lmul vt))))
           (list w r)))
       (hash-same-lmul (vt)
         (let ((b (single-letter (et-base-ty (vt-elem-ty vt))))
               (r (lm-ratio (vt-lmul vt))))
           (list b r)))
       (is-float (vt)
         (eq (single-letter (et-base-ty (vt-elem-ty vt))) #\f)))
  (let* ((table-same-sew
           (classify (enumerate-vector-types)
                     #'hash-same-sew :test #'equal))
         (table-same-lmul
           (classify (remove-if #'is-float (enumerate-vector-types))
                     #'hash-same-lmul :test #'equal))
         (seqs (mapcar #'cadr (append table-same-sew table-same-lmul))))
    (loop for sublist in seqs do
      (loop for pair in (combination sublist) do
        (apply #'rvv-reinterpret pair)
        (apply #'rvv-reinterpret (reverse pair))))))

;; Vector Slideup and Slidedown Functions
(loop for vt in (enumerate-vector-types) do
  (let ((sf (suffix vt)))
    (rvv-intrinsic
     vt ("vslideup_vx_~a" sf) ((vt "src") (size_t "offset")))))

(loop for mt in (enumerate-mask-types) do
  (let ((abbr (unified-abbr mt)))
    (rvv-intrinsic
     mt ("vmset_m_~a" abbr) ())))
