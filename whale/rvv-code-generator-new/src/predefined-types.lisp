(defun enumerate-base-types ()
  (list
   (make-instance 'base-type :c "int" :s #\i :g #\i)
   (make-instance 'base-type :c "unsigned" :s #\u :g #\i)
   (make-instance 'base-type :c "float" :s #\f :g #\f)))

(defun enumerate-sews (&optional base-type)
  (flet ((test (sew)
               (and (string= (single-letter base-type) #\f)
                    (< (slot-value sew 'width) 32))))
    (if base-type
        (remove-if #'test (enumerate-sews))
      (loop for width in '(8 16 32 64)
            collect (make-instance 'std-element-width :w width)))))

(defconstant ELEN 64)

(defun enumerate-lmuls (&optional sew)
  (flet ((test (lmul)
               (with-slots (width) sew
                 (with-slots (ratio) lmul
                   (and (is-fract lmul) (< (* ELEN ratio) width))))))
    (if sew
        (remove-if #'test (enumerate-lmuls))
      (loop for ratio in '(1 2 4 8)
            collect (make-instance 'length-multiplier :r ratio)))))

(defun enumerate-element-types ()
  (loop for bt in (enumerate-base-types)
        append (loop for sw in (enumerate-sews bt)
                     collect (make-instance 'element-type :b bt :s sw))))

(defun enumerate-vector-types ()
  (loop for et in (enumerate-element-types)
        append (loop for lmul in (enumerate-lmuls (slot-value et 'sew))
                     collect (make-instance 'vector-type :e et :lmul lmul))))

(defun enumerate-mask-types ()
  (loop for size in '(1 2 4 8 16 32 64)
        collect (make-instance 'mask-type :s size)))

(defconstant void_t
  (make-instance 'scalar-specifier
                 :b "void" :a "v" :o nil :lv "llvm_void_ty"))

(defconstant size_t
  (make-instance 'scalar-specifier
                 :b "size_t" :a "z" :o t :lv "llvm_anyint_ty"))

(defconstant ptrdiff_t
  (make-instance 'scalar-specifier
                 :b "ptrdiff_t" :a "Y" :o t :lv "llvm_anyptr_ty"))

(defun ptr-to (element-type)
  (make-instance 'scalar-specifier
                 :b (c-type element-type)
                 :a (abbr element-type)
                 :o t
                 :lv "llvm_anyptr_ty"
                 :m '("*")))

(defun const-ptr-to (element-type)
  (make-instance 'scalar-specifier
                 :b (c-type element-type)
                 :a (abbr element-type)
                 :o t
                 :lv "llvm_anyptr_ty"
                 :m '("*" "const")))
