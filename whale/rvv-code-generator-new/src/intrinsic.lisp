(defclass intrinsic nil
  ((name
    :initarg :n
    :reader intr-name)
   (return-type
    :initarg :r
    :reader intr-return-ty)
   (parameters
    :initarg :pas
    :reader intr-params)
   (attributes
    :initarg :as
    :initform ""
    :reader intr-attrs)
   (properties
    :initarg :prs
    :initform nil
    :reader intr-props)
   (body
    :initarg :b
    :initform nil
    :reader intr-body)
   (gen-clang-intrinsic
    :initarg :gci
    :initform t
    :reader intr-gci)
   (gen-clang-builtin
    :initarg :gcb
    :initform t
    :reader intr-gcb)
   (gen-llvm-intrinsic
    :initarg :gli
    :initform t
    :reader intr-gli)))

(defun parameter-types (intrinsic)
  (mapcar #'car (slot-value intrinsic 'parameters)))

(defun parameter-names (intrinsic)
  (mapcar #'cadr (slot-value intrinsic 'parameters)))

(defun signature (intrinsic)
  (cons (slot-value intrinsic 'return-type)
        (parameter-types intrinsic)))


(defun to-clang-intrinsic (intrinsic)
  (join
   (gather
    (take-all "static __attribute__((always_inline))" #\newline)
    (with-slots (name return-type) intrinsic
      (take-all (c-type return-type) " " name "("))
    (with-slots (parameters) intrinsic
      (take
       (interleave ", " (loop for (type name) in parameters
                              collect (format nil "~A ~A" (c-type type) name))))
      (take-all ") {" #\newline))
    (with-slots (name return-type body) intrinsic
      (if body
          (take-all body)
        (progn
          (take-all "  ")
          (if (not (is-void return-type))
              (take-all "return "))
          (take-all "__builtin_riscv_" name "(")
          (take (interleave ", " (parameter-names intrinsic)))
          (take-all ");"))))
    (take-all #\newline "}"))))

(defun to-clang-builtin (intrinsic)
  (join
   (gather
    (take-all "RISCVBuiltin(")
    (let* ((return-type (intr-return-ty intrinsic))
           (pts (cons return-type (parameter-types intrinsic)))
           (attributes (intr-attrs intrinsic)))
      (take-all (intr-name intrinsic) ", \"")
      (take-all (join (mapcar #'abbr pts)))
      (take-all "\", \"")
      (if attributes
          (take-all attributes))
      (take-all "\", ")
      (loop for type in pts and id from 0
            if (is-overloaded type) do
              (take-all id ", "))
      (take-all ")")))))

(defun to-llvm-intrinsic (intrinsic)
  (join
   (gather
    (with-slots (name return-type) intrinsic
      (take-all "def int_riscv_" name #\newline)
      (take-all "  : Intrinsic<[")
      (if (not (is-void return-type))
          (take-all (llvm-type return-type))))
    (take-all "]," #\newline)
    (take-all "              [")
    (let ((pts (parameter-types intrinsic)))
      (take (interleave ", " (mapcar #'llvm-type pts))))
    (take-all "]," #\newline)
    (take-all "              [")
    (with-slots (properties) intrinsic
      (take (interleave "," properties)))
    (take-all "]>;"))))
