(defmacro gather (&rest body)
  (let ((buffer (gensym)))
    `(let ((,buffer))
       (flet ((take (items)
                    (loop for it in items
                          do (push it ,buffer))))
         (flet ((take-all (&rest items) (take items)))
           ,@body
           (reverse ,buffer))))))

(defun interleave (delimiter list)
  (if list
      (cons (car list)
            (loop for item in (cdr list)
                  collect delimiter
                  collect item))))

(defun join (items)
  (format nil "~{~a~}" items))

(defun combination (items)
  (loop for sublist in (maplist #'identity items)
        append (let ((head (car sublist))
                     (rest (cdr sublist)))
                 (loop for it in rest
                       collect (list head it)))))

(defun classify (items hash &key test)
  (gather
   (let ((table (make-hash-table :test test)))
     (loop for it in items do
       (push it (gethash (funcall hash it) table)))
     (maphash (lambda (k v) (take-all (list k v))) table))))
