(defclass std-element-width nil
  ((width
    :initarg :w
    :reader sw-width)))

(defmethod encoding ((sew std-element-width))
  (with-slots (width) sew
    ;; ((log2(width) - 3) << 2) in c
    (ash (- (round (log width 2)) 3) 2)))

(defmethod double-time ((sew std-element-width))
  (with-slots (width) sew
    (make-instance 'std-element-width :w (width * 2))))

(defmethod definition ((object std-element-width))
  (with-slots (width) object
    (format nil "#define _E~s ~s" width (encoding object))))
