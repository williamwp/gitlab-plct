(defclass vector-type (type-specifier)
  ((element-type
    :initarg :e
    :reader vt-elem-ty)
   (lmul
    :initarg :lmul
    :reader vt-lmul)))

(defmethod c-type ((specifier vector-type))
  (with-slots (element-type lmul) specifier
    (format nil "v~am~a_t" (unified element-type) (lower-case lmul))))

(defmethod abbr ((specifier vector-type))
  (with-slots (element-type lmul) specifier
    (format nil "q~a~a" (lower-case lmul) (abbr element-type))))

(defmethod llvm-type ((specifier vector-type))
  (with-slots (element-type lmul) specifier
    (format nil "llvm_nxv~a~a_ty"
            (lower-case lmul) (general-unified-abbr element-type))))

(defun suffix (vector-type)
  (with-slots (element-type lmul) vector-type
    (format nil "~am~a" (unified-abbr element-type) (lower-case lmul))))

(defmethod double-time ((specifier vector-type))
  (with-slots (element-type lmul) specifier
    (let ((new-element-type (double-time element-type))
          (new-lmul (double-time lmul)))
      (make-instance 'vector-type :e new-element-type :lmul new-lmul))))

(defun corresponding-mask-type (vector-type)
  (let* ((et (vt-elem-ty vector-type))
         (width (sw-width (et-sew et)))
         (ratio (lm-ratio (vt-lmul vector-type))))
    (make-instance 'mask-type :s (/ width ratio))))

(defmethod definition ((object vector-type))
  (with-slots (element-type lmul) object
    (with-slots (base-type sew) element-type
      (join
       (gather
        (take-all "typedef __attribute__((riscv_vector_type(")
        (take-all (slot-value sew 'width))
        (take-all ", ")
        (take-all (multiplier lmul))
        (take-all ", ")
        (take-all (if (is-fract lmul) 1 0))
        (take-all "))) ")
        (take-all (slot-value base-type 'c-type))
        (take-all " ")
        (take-all (c-type object))
        (take-all ";"))))))
