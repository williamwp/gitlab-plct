(defclass base-type nil
  ((c-type
    :initarg :c)
   (single-letter
    :initarg :s
    :reader single-letter)
   (general-single-letter
    :initarg :g
    :reader general-single-letter)))

(defmethod c-type ((specifier base-type))
  (slot-value specifier 'c-type))
