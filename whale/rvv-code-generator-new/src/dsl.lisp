(defvar *intrinsics* nil)

(defun to-callable-list (list)
  (cons 'list
        (loop for sublist in list
              collect (cons 'list sublist))))

(defun parse-properties (spec)
  (gather
   (loop for c across spec do
     (ecase c
       (#\s (take-all "IntrHasSideEffects"))
       (#\n (take-all "IntrNoMem"))
       (#\r (take-all "IntrReadMem"))
       (#\w (take-all "IntrWriteMem"))))))

(defun maybe-formatted (object)
  (if (and object (listp object))
      (cons 'format (cons nil object))
      object))

(defmacro rvv-intrinsic (return-type name parameters
                         &key attributes (properties "n") body
                           (gci 1) (gcb 1) (gli 1)
                           ((:mask main-vec-ty)) maskedoff)
  (let ((name (maybe-formatted name))
        (parameters (to-callable-list parameters))
        (body (maybe-formatted body))
        (properties (cons 'list (parse-properties properties))))
    `(progn
       (if ,main-vec-ty
           (flet ((add-suffix (str) (concatenate 'string str "_m")))
             (let* ((name (if (stringp ,name)
                              (add-suffix ,name)
                              (cons (add-suffix (car ,name)) (cdr ,name))))
                    (mask-ty (corresponding-mask-type ,main-vec-ty))
                    (parameters (cons (list mask-ty "mask")
                                      (if ,maskedoff
                                          (cons (list ,main-vec-ty "maskedoff") ,parameters)
                                          ,parameters)))
                    (intrinsic (make-instance 'intrinsic
                                              :n name :r ,return-type :pas parameters
                                              :as ,attributes :prs ,properties :b ,body
                                              :gci ,gci :gcb ,gcb :gli ,gli)))
               (push intrinsic *intrinsics*))))
       (let ((intrinsic (make-instance 'intrinsic
                                       :n ,name :r ,return-type :pas ,parameters
                                       :as ,attributes :prs ,properties :b ,body
                                       :gci ,gci :gcb ,gcb :gli ,gli)))
         (push intrinsic *intrinsics*)))))
