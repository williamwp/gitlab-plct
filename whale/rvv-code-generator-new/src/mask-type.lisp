(defclass mask-type (type-specifier)
  ((size
    :initarg :s)))

(defmethod c-type ((specifier mask-type))
  (format nil "vbool~a_t" (slot-value specifier 'size)))

(defmethod abbr ((specifier mask-type))
  (format nil "q~ab" (slot-value specifier 'size)))

(defmethod llvm-type ((specifier mask-type)) "llvm_nxv1i1_ty")

(defmethod unified-abbr ((type mask-type))
  (format nil "b~a" (slot-value type 'size)))

(defmethod definition ((object mask-type))
  (format nil "typedef __attribute__((riscv_mask_type(~a))) bool ~a;"
          (slot-value object 'size)
          (c-type object)))
