(load "src/utils.lisp")
(load "src/interface.lisp")
(load "src/base-type.lisp")
(load "src/std-element-width.lisp")
(load "src/length-multiplier.lisp")
(load "src/element-type.lisp")
(load "src/vector-type.lisp")
(load "src/mask-type.lisp")
(load "src/scalar-specifier.lisp")
(load "src/intrinsic.lisp")
(load "src/predefined-types.lisp")
(load "src/dsl.lisp")
(load "src/declarations.lisp")

(defun interleave-newline (items)
  (interleave #\newline items))

(defun emit-clang-intrinsics (stream intrinsics)
  (format stream "~{~a~}"
          (gather
           (take
            (interleave-newline
             (list
              "#ifndef __RISCV_VECTOR_H__"
              "#define __RISCV_VECTOR_H__"
              ""
              "#include <stdbool.h>"
              "#include <stddef.h>"
              "#include <stdint.h>"
              "")))
           (flet ((take-defs (items)
                    (loop for it in items do
                      (take-all (definition it) #\newline #\newline))))
             (take-defs (enumerate-sews))
             (take-all #\newline)
             (take-defs (enumerate-lmuls))
             (take-all #\newline)
             (take-defs (enumerate-vector-types))
             (take-all #\newline)
             (take-defs (enumerate-mask-types)))
           (take-all #\newline)
           (loop for intr in intrinsics do
             (when (intr-gci intr)
               (take-all (to-clang-intrinsic intr) #\newline #\newline)))
           (take-all "#endif"))))

(defun emit-clang-builtins (stream intrinsics)
  (format stream "~{~a~}"
          (gather
           (loop for it in intrinsics do
             (when (intr-gcb it)
               (take-all (to-clang-builtin it) #\newline #\newline)))
           (take-all #\newline)
           (take-all "#undef RISCVBuiltin"))))

(defun emit-llvm-intrinsics (stream intrinsics)
  (format stream "~{~a~}"
          (gather
           (loop for it in intrinsics do
             (when (intr-gli it)
               (take-all (to-llvm-intrinsic it) #\newline #\newline))))))

(defun find-sublist (item list &key test)
  (flet ((pred (sublist)
           (funcall test item (car sublist))))
    (car (remove-if-not #'pred (maplist #'identity list)))))

(let ((args sb-ext:*posix-argv*))
  (flet ((supplied (option)
           (find-sublist option args :test #'string=)))
    (let ((intrinsics (reverse *intrinsics*))
          (path-to-llvm-project (supplied "--path-to-llvm-project")))
      (when (supplied "--print-clang-intrinsics")
        (emit-clang-intrinsics t intrinsics))
      (when (supplied "--print-clang-builtins")
        (emit-clang-builtins t intrinsics))
      (when (supplied "--print-llvm-intrinsics")
        (emit-llvm-intrinsics t intrinsics))
      (flet ((abs-path (relative-path)
               (join (list (cadr path-to-llvm-project) "/" relative-path))))
        (flet ((write-all (emitter relative-path)
                 (with-open-file
                     (stream (abs-path relative-path)
                             :direction :output
                             :if-exists :supersede
                             :if-does-not-exist :create)
                   (funcall emitter stream intrinsics))))
          (when (supplied "--write-clang-intrinsics")
            (write-all #'emit-clang-intrinsics
                       "clang/lib/Headers/riscv_vector.h"))
          (when (supplied "--write-clang-builtins")
            (write-all #'emit-clang-builtins
                       "clang/include/clang/Basic/riscv_builtin.h")
            (write-all #'emit-clang-builtins
                       "clang/lib/CodeGen/riscv_builtin.h"))
          (when (supplied "--write-llvm-intrinsics")
            (write-all #'emit-llvm-intrinsics
                       "llvm/include/llvm/IR/IntrinsicsRISCVVector.td")))))))
