; RUN: llc -mtriple=riscv64 -mattr=+v -verify-machineinstrs < %s \
; RUN:   | FileCheck -check-prefix=CHECK %s

declare <vscale x 32 x i16> @llvm.riscv.vwadd.vv.i16m8(<vscale x 32 x i8>, <vscale x 32 x i8>);
define <vscale x 32 x i16> @vwadd_vv_i16m8(<vscale x 32 x i8> %0, <vscale x 32 x i8> %1) {
entry:
; CHECK-LABEL: vwadd_vv_i16m8
; CHECK: vwadd.vv v0, v0, v1
; CHECK: ret
%a =  tail call <vscale x 32 x i16> @llvm.riscv.vwadd.vv.i16m8(<vscale x 32 x i8> %0, <vscale x 32 x i8> %1)
ret <vscale x 32 x i16> %a
}

declare <vscale x 32 x i16> @llvm.riscv.vwaddu.vv.u16m8(<vscale x 32 x i8>, <vscale x 32 x i8>);
define <vscale x 32 x i16> @vwaddu_vv_u16m8(<vscale x 32 x i8> %0, <vscale x 32 x i8> %1) {
entry:
; CHECK-LABEL: vwaddu_vv_u16m8
; CHECK: vwaddu.vv v0, v0, v1
; CHECK: ret
%a =  tail call <vscale x 32 x i16> @llvm.riscv.vwaddu.vv.u16m8(<vscale x 32 x i8> %0, <vscale x 32 x i8> %1)
ret <vscale x 32 x i16> %a
}

