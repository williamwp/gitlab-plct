; RUN: llc -mtriple=riscv64 -mattr=+v -verify-machineinstrs < %s \
; RUN:   | FileCheck -check-prefix=CHECK %s

declare <vscale x 4 x i16> @llvm.riscv.vwsub.vv.i16m2(<vscale x 8 x i8>, <vscale x 8 x i8>);
define <vscale x 4 x i16> @vwsub_vv_i16m2(<vscale x 8 x i8> %0, <vscale x 8 x i8> %1) {
entry:
; CHECK-LABEL: vwsub_vv_i16m2
; CHECK: vwsub.vv v0, v0, v1
; CHECK: ret
%a =  tail call <vscale x 4 x i16> @llvm.riscv.vwsub.vv.i16m2(<vscale x 8 x i8> %0, <vscale x 8 x i8> %1)
ret <vscale x 4 x i16> %a
}

declare <vscale x 4 x i16> @llvm.riscv.vwsubu.vv.u16m2(<vscale x 8 x i8>, <vscale x 8 x i8>);
define <vscale x 4 x i16> @vwsubu_vv_u16m2(<vscale x 8 x i8> %0, <vscale x 8 x i8> %1) {
entry:
; CHECK-LABEL: vwsubu_vv_u16m2
; CHECK: vwsubu.vv v0, v0, v1
; CHECK: ret
%a =  tail call <vscale x 4 x i16> @llvm.riscv.vwsubu.vv.u16m2(<vscale x 8 x i8> %0, <vscale x 8 x i8> %1)
ret <vscale x 4 x i16> %a
}

