RISCVBuiltin(vsetvl, "zzz", "", 0, 1, 2, )

RISCVBuiltin(vsetvlmax, "zz", "", 0, 1, )

RISCVBuiltin(vle8_v_i8m1, "q8ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m1, "vq8ScSc*", "", 2, )

RISCVBuiltin(vle8_v_i8m2, "q16ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m2, "vq16ScSc*", "", 2, )

RISCVBuiltin(vle8_v_i8m4, "q32ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m4, "vq32ScSc*", "", 2, )

RISCVBuiltin(vle8_v_i8m8, "q64ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m8, "vq64ScSc*", "", 2, )

RISCVBuiltin(vle16_v_i16m1, "q4SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m1, "vq4SsSs*", "", 2, )

RISCVBuiltin(vle16_v_i16m2, "q8SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m2, "vq8SsSs*", "", 2, )

RISCVBuiltin(vle16_v_i16m4, "q16SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m4, "vq16SsSs*", "", 2, )

RISCVBuiltin(vle16_v_i16m8, "q32SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m8, "vq32SsSs*", "", 2, )

RISCVBuiltin(vle32_v_i32m1, "q2ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m1, "vq2ZiZi*", "", 2, )

RISCVBuiltin(vle32_v_i32m2, "q4ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m2, "vq4ZiZi*", "", 2, )

RISCVBuiltin(vle32_v_i32m4, "q8ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m4, "vq8ZiZi*", "", 2, )

RISCVBuiltin(vle32_v_i32m8, "q16ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m8, "vq16ZiZi*", "", 2, )

RISCVBuiltin(vle64_v_i64m1, "q1WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m1, "vq1WiWi*", "", 2, )

RISCVBuiltin(vle64_v_i64m2, "q2WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m2, "vq2WiWi*", "", 2, )

RISCVBuiltin(vle64_v_i64m4, "q4WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m4, "vq4WiWi*", "", 2, )

RISCVBuiltin(vle64_v_i64m8, "q8WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m8, "vq8WiWi*", "", 2, )

RISCVBuiltin(vle8_v_u8m1, "q8UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m1, "vq8UcUc*", "", 2, )

RISCVBuiltin(vle8_v_u8m2, "q16UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m2, "vq16UcUc*", "", 2, )

RISCVBuiltin(vle8_v_u8m4, "q32UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m4, "vq32UcUc*", "", 2, )

RISCVBuiltin(vle8_v_u8m8, "q64UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m8, "vq64UcUc*", "", 2, )

RISCVBuiltin(vle16_v_u16m1, "q4UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m1, "vq4UsUs*", "", 2, )

RISCVBuiltin(vle16_v_u16m2, "q8UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m2, "vq8UsUs*", "", 2, )

RISCVBuiltin(vle16_v_u16m4, "q16UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m4, "vq16UsUs*", "", 2, )

RISCVBuiltin(vle16_v_u16m8, "q32UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m8, "vq32UsUs*", "", 2, )

RISCVBuiltin(vle32_v_u32m1, "q2UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m1, "vq2UZiUZi*", "", 2, )

RISCVBuiltin(vle32_v_u32m2, "q4UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m2, "vq4UZiUZi*", "", 2, )

RISCVBuiltin(vle32_v_u32m4, "q8UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m4, "vq8UZiUZi*", "", 2, )

RISCVBuiltin(vle32_v_u32m8, "q16UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m8, "vq16UZiUZi*", "", 2, )

RISCVBuiltin(vle64_v_u64m1, "q1UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m1, "vq1UWiUWi*", "", 2, )

RISCVBuiltin(vle64_v_u64m2, "q2UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m2, "vq2UWiUWi*", "", 2, )

RISCVBuiltin(vle64_v_u64m4, "q4UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m4, "vq4UWiUWi*", "", 2, )

RISCVBuiltin(vle64_v_u64m8, "q8UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m8, "vq8UWiUWi*", "", 2, )

RISCVBuiltin(vle32_v_f32m1, "q2ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m1, "vq2ff*", "", 2, )

RISCVBuiltin(vle32_v_f32m2, "q4ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m2, "vq4ff*", "", 2, )

RISCVBuiltin(vle32_v_f32m4, "q8ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m4, "vq8ff*", "", 2, )

RISCVBuiltin(vle32_v_f32m8, "q16ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m8, "vq16ff*", "", 2, )

RISCVBuiltin(vle64_v_f64m1, "q1ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m1, "vq1dd*", "", 2, )

RISCVBuiltin(vle64_v_f64m2, "q2ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m2, "vq2dd*", "", 2, )

RISCVBuiltin(vle64_v_f64m4, "q4ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m4, "vq4dd*", "", 2, )

RISCVBuiltin(vle64_v_f64m8, "q8ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m8, "vq8dd*", "", 2, )

RISCVBuiltin(vmv_x_s_i8m1_i8, "Scq8Sc", "", )

RISCVBuiltin(vmv_s_x_i8m1, "q8Scq8ScSc", "", )

RISCVBuiltin(vmv_x_s_i8m2_i8, "Scq16Sc", "", )

RISCVBuiltin(vmv_s_x_i8m2, "q16Scq16ScSc", "", )

RISCVBuiltin(vmv_x_s_i8m4_i8, "Scq32Sc", "", )

RISCVBuiltin(vmv_s_x_i8m4, "q32Scq32ScSc", "", )

RISCVBuiltin(vmv_x_s_i8m8_i8, "Scq64Sc", "", )

RISCVBuiltin(vmv_s_x_i8m8, "q64Scq64ScSc", "", )

RISCVBuiltin(vmv_x_s_i16m1_i16, "Ssq4Ss", "", )

RISCVBuiltin(vmv_s_x_i16m1, "q4Ssq4SsSs", "", )

RISCVBuiltin(vmv_x_s_i16m2_i16, "Ssq8Ss", "", )

RISCVBuiltin(vmv_s_x_i16m2, "q8Ssq8SsSs", "", )

RISCVBuiltin(vmv_x_s_i16m4_i16, "Ssq16Ss", "", )

RISCVBuiltin(vmv_s_x_i16m4, "q16Ssq16SsSs", "", )

RISCVBuiltin(vmv_x_s_i16m8_i16, "Ssq32Ss", "", )

RISCVBuiltin(vmv_s_x_i16m8, "q32Ssq32SsSs", "", )

RISCVBuiltin(vmv_x_s_i32m1_i32, "Ziq2Zi", "", )

RISCVBuiltin(vmv_s_x_i32m1, "q2Ziq2ZiZi", "", )

RISCVBuiltin(vmv_x_s_i32m2_i32, "Ziq4Zi", "", )

RISCVBuiltin(vmv_s_x_i32m2, "q4Ziq4ZiZi", "", )

RISCVBuiltin(vmv_x_s_i32m4_i32, "Ziq8Zi", "", )

RISCVBuiltin(vmv_s_x_i32m4, "q8Ziq8ZiZi", "", )

RISCVBuiltin(vmv_x_s_i32m8_i32, "Ziq16Zi", "", )

RISCVBuiltin(vmv_s_x_i32m8, "q16Ziq16ZiZi", "", )

RISCVBuiltin(vmv_x_s_i64m1_i64, "Wiq1Wi", "", )

RISCVBuiltin(vmv_s_x_i64m1, "q1Wiq1WiWi", "", )

RISCVBuiltin(vmv_x_s_i64m2_i64, "Wiq2Wi", "", )

RISCVBuiltin(vmv_s_x_i64m2, "q2Wiq2WiWi", "", )

RISCVBuiltin(vmv_x_s_i64m4_i64, "Wiq4Wi", "", )

RISCVBuiltin(vmv_s_x_i64m4, "q4Wiq4WiWi", "", )

RISCVBuiltin(vmv_x_s_i64m8_i64, "Wiq8Wi", "", )

RISCVBuiltin(vmv_s_x_i64m8, "q8Wiq8WiWi", "", )

RISCVBuiltin(vmv_x_s_u8m1_u8, "Ucq8Uc", "", )

RISCVBuiltin(vmv_s_x_u8m1, "q8Ucq8UcUc", "", )

RISCVBuiltin(vmv_x_s_u8m2_u8, "Ucq16Uc", "", )

RISCVBuiltin(vmv_s_x_u8m2, "q16Ucq16UcUc", "", )

RISCVBuiltin(vmv_x_s_u8m4_u8, "Ucq32Uc", "", )

RISCVBuiltin(vmv_s_x_u8m4, "q32Ucq32UcUc", "", )

RISCVBuiltin(vmv_x_s_u8m8_u8, "Ucq64Uc", "", )

RISCVBuiltin(vmv_s_x_u8m8, "q64Ucq64UcUc", "", )

RISCVBuiltin(vmv_x_s_u16m1_u16, "Usq4Us", "", )

RISCVBuiltin(vmv_s_x_u16m1, "q4Usq4UsUs", "", )

RISCVBuiltin(vmv_x_s_u16m2_u16, "Usq8Us", "", )

RISCVBuiltin(vmv_s_x_u16m2, "q8Usq8UsUs", "", )

RISCVBuiltin(vmv_x_s_u16m4_u16, "Usq16Us", "", )

RISCVBuiltin(vmv_s_x_u16m4, "q16Usq16UsUs", "", )

RISCVBuiltin(vmv_x_s_u16m8_u16, "Usq32Us", "", )

RISCVBuiltin(vmv_s_x_u16m8, "q32Usq32UsUs", "", )

RISCVBuiltin(vmv_x_s_u32m1_u32, "UZiq2UZi", "", )

RISCVBuiltin(vmv_s_x_u32m1, "q2UZiq2UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u32m2_u32, "UZiq4UZi", "", )

RISCVBuiltin(vmv_s_x_u32m2, "q4UZiq4UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u32m4_u32, "UZiq8UZi", "", )

RISCVBuiltin(vmv_s_x_u32m4, "q8UZiq8UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u32m8_u32, "UZiq16UZi", "", )

RISCVBuiltin(vmv_s_x_u32m8, "q16UZiq16UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u64m1_u64, "UWiq1UWi", "", )

RISCVBuiltin(vmv_s_x_u64m1, "q1UWiq1UWiUWi", "", )

RISCVBuiltin(vmv_x_s_u64m2_u64, "UWiq2UWi", "", )

RISCVBuiltin(vmv_s_x_u64m2, "q2UWiq2UWiUWi", "", )

RISCVBuiltin(vmv_x_s_u64m4_u64, "UWiq4UWi", "", )

RISCVBuiltin(vmv_s_x_u64m4, "q4UWiq4UWiUWi", "", )

RISCVBuiltin(vmv_x_s_u64m8_u64, "UWiq8UWi", "", )

RISCVBuiltin(vmv_s_x_u64m8, "q8UWiq8UWiUWi", "", )

RISCVBuiltin(vfmv_f_s_f32m1_f32, "fq2f", "", )

RISCVBuiltin(vfmv_s_f_f32m1, "q2fq2ff", "", )

RISCVBuiltin(vfmv_f_s_f32m2_f32, "fq4f", "", )

RISCVBuiltin(vfmv_s_f_f32m2, "q4fq4ff", "", )

RISCVBuiltin(vfmv_f_s_f32m4_f32, "fq8f", "", )

RISCVBuiltin(vfmv_s_f_f32m4, "q8fq8ff", "", )

RISCVBuiltin(vfmv_f_s_f32m8_f32, "fq16f", "", )

RISCVBuiltin(vfmv_s_f_f32m8, "q16fq16ff", "", )

RISCVBuiltin(vfmv_f_s_f64m1_f64, "dq1d", "", )

RISCVBuiltin(vfmv_s_f_f64m1, "q1dq1dd", "", )

RISCVBuiltin(vfmv_f_s_f64m2_f64, "dq2d", "", )

RISCVBuiltin(vfmv_s_f_f64m2, "q2dq2dd", "", )

RISCVBuiltin(vfmv_f_s_f64m4_f64, "dq4d", "", )

RISCVBuiltin(vfmv_s_f_f64m4, "q4dq4dd", "", )

RISCVBuiltin(vfmv_f_s_f64m8_f64, "dq8d", "", )

RISCVBuiltin(vfmv_s_f_f64m8, "q8dq8dd", "", )

RISCVBuiltin(vzero_i8m1, "q8Sc", "", )

RISCVBuiltin(vundefined_i8m1, "q8Sc", "", )

RISCVBuiltin(vzero_i8m2, "q16Sc", "", )

RISCVBuiltin(vundefined_i8m2, "q16Sc", "", )

RISCVBuiltin(vzero_i8m4, "q32Sc", "", )

RISCVBuiltin(vundefined_i8m4, "q32Sc", "", )

RISCVBuiltin(vzero_i8m8, "q64Sc", "", )

RISCVBuiltin(vundefined_i8m8, "q64Sc", "", )

RISCVBuiltin(vzero_i16m1, "q4Ss", "", )

RISCVBuiltin(vundefined_i16m1, "q4Ss", "", )

RISCVBuiltin(vzero_i16m2, "q8Ss", "", )

RISCVBuiltin(vundefined_i16m2, "q8Ss", "", )

RISCVBuiltin(vzero_i16m4, "q16Ss", "", )

RISCVBuiltin(vundefined_i16m4, "q16Ss", "", )

RISCVBuiltin(vzero_i16m8, "q32Ss", "", )

RISCVBuiltin(vundefined_i16m8, "q32Ss", "", )

RISCVBuiltin(vzero_i32m1, "q2Zi", "", )

RISCVBuiltin(vundefined_i32m1, "q2Zi", "", )

RISCVBuiltin(vzero_i32m2, "q4Zi", "", )

RISCVBuiltin(vundefined_i32m2, "q4Zi", "", )

RISCVBuiltin(vzero_i32m4, "q8Zi", "", )

RISCVBuiltin(vundefined_i32m4, "q8Zi", "", )

RISCVBuiltin(vzero_i32m8, "q16Zi", "", )

RISCVBuiltin(vundefined_i32m8, "q16Zi", "", )

RISCVBuiltin(vzero_i64m1, "q1Wi", "", )

RISCVBuiltin(vundefined_i64m1, "q1Wi", "", )

RISCVBuiltin(vzero_i64m2, "q2Wi", "", )

RISCVBuiltin(vundefined_i64m2, "q2Wi", "", )

RISCVBuiltin(vzero_i64m4, "q4Wi", "", )

RISCVBuiltin(vundefined_i64m4, "q4Wi", "", )

RISCVBuiltin(vzero_i64m8, "q8Wi", "", )

RISCVBuiltin(vundefined_i64m8, "q8Wi", "", )

RISCVBuiltin(vzero_u8m1, "q8Uc", "", )

RISCVBuiltin(vundefined_u8m1, "q8Uc", "", )

RISCVBuiltin(vzero_u8m2, "q16Uc", "", )

RISCVBuiltin(vundefined_u8m2, "q16Uc", "", )

RISCVBuiltin(vzero_u8m4, "q32Uc", "", )

RISCVBuiltin(vundefined_u8m4, "q32Uc", "", )

RISCVBuiltin(vzero_u8m8, "q64Uc", "", )

RISCVBuiltin(vundefined_u8m8, "q64Uc", "", )

RISCVBuiltin(vzero_u16m1, "q4Us", "", )

RISCVBuiltin(vundefined_u16m1, "q4Us", "", )

RISCVBuiltin(vzero_u16m2, "q8Us", "", )

RISCVBuiltin(vundefined_u16m2, "q8Us", "", )

RISCVBuiltin(vzero_u16m4, "q16Us", "", )

RISCVBuiltin(vundefined_u16m4, "q16Us", "", )

RISCVBuiltin(vzero_u16m8, "q32Us", "", )

RISCVBuiltin(vundefined_u16m8, "q32Us", "", )

RISCVBuiltin(vzero_u32m1, "q2UZi", "", )

RISCVBuiltin(vundefined_u32m1, "q2UZi", "", )

RISCVBuiltin(vzero_u32m2, "q4UZi", "", )

RISCVBuiltin(vundefined_u32m2, "q4UZi", "", )

RISCVBuiltin(vzero_u32m4, "q8UZi", "", )

RISCVBuiltin(vundefined_u32m4, "q8UZi", "", )

RISCVBuiltin(vzero_u32m8, "q16UZi", "", )

RISCVBuiltin(vundefined_u32m8, "q16UZi", "", )

RISCVBuiltin(vzero_u64m1, "q1UWi", "", )

RISCVBuiltin(vundefined_u64m1, "q1UWi", "", )

RISCVBuiltin(vzero_u64m2, "q2UWi", "", )

RISCVBuiltin(vundefined_u64m2, "q2UWi", "", )

RISCVBuiltin(vzero_u64m4, "q4UWi", "", )

RISCVBuiltin(vundefined_u64m4, "q4UWi", "", )

RISCVBuiltin(vzero_u64m8, "q8UWi", "", )

RISCVBuiltin(vundefined_u64m8, "q8UWi", "", )

RISCVBuiltin(vzero_f32m1, "q2f", "", )

RISCVBuiltin(vundefined_f32m1, "q2f", "", )

RISCVBuiltin(vzero_f32m2, "q4f", "", )

RISCVBuiltin(vundefined_f32m2, "q4f", "", )

RISCVBuiltin(vzero_f32m4, "q8f", "", )

RISCVBuiltin(vundefined_f32m4, "q8f", "", )

RISCVBuiltin(vzero_f32m8, "q16f", "", )

RISCVBuiltin(vundefined_f32m8, "q16f", "", )

RISCVBuiltin(vzero_f64m1, "q1d", "", )

RISCVBuiltin(vundefined_f64m1, "q1d", "", )

RISCVBuiltin(vzero_f64m2, "q2d", "", )

RISCVBuiltin(vundefined_f64m2, "q2d", "", )

RISCVBuiltin(vzero_f64m4, "q4d", "", )

RISCVBuiltin(vundefined_f64m4, "q4d", "", )

RISCVBuiltin(vzero_f64m8, "q8d", "", )

RISCVBuiltin(vundefined_f64m8, "q8d", "", )

RISCVBuiltin(vmv_v_v_i8m1, "q8Scq8Sc", "", )

RISCVBuiltin(vmv_v_x_i8m1, "q8ScSc", "", )

RISCVBuiltin(vmv_v_v_i8m2, "q16Scq16Sc", "", )

RISCVBuiltin(vmv_v_x_i8m2, "q16ScSc", "", )

RISCVBuiltin(vmv_v_v_i8m4, "q32Scq32Sc", "", )

RISCVBuiltin(vmv_v_x_i8m4, "q32ScSc", "", )

RISCVBuiltin(vmv_v_v_i8m8, "q64Scq64Sc", "", )

RISCVBuiltin(vmv_v_x_i8m8, "q64ScSc", "", )

RISCVBuiltin(vmv_v_v_i16m1, "q4Ssq4Ss", "", )

RISCVBuiltin(vmv_v_x_i16m1, "q4SsSs", "", )

RISCVBuiltin(vmv_v_v_i16m2, "q8Ssq8Ss", "", )

RISCVBuiltin(vmv_v_x_i16m2, "q8SsSs", "", )

RISCVBuiltin(vmv_v_v_i16m4, "q16Ssq16Ss", "", )

RISCVBuiltin(vmv_v_x_i16m4, "q16SsSs", "", )

RISCVBuiltin(vmv_v_v_i16m8, "q32Ssq32Ss", "", )

RISCVBuiltin(vmv_v_x_i16m8, "q32SsSs", "", )

RISCVBuiltin(vmv_v_v_i32m1, "q2Ziq2Zi", "", )

RISCVBuiltin(vmv_v_x_i32m1, "q2ZiZi", "", )

RISCVBuiltin(vmv_v_v_i32m2, "q4Ziq4Zi", "", )

RISCVBuiltin(vmv_v_x_i32m2, "q4ZiZi", "", )

RISCVBuiltin(vmv_v_v_i32m4, "q8Ziq8Zi", "", )

RISCVBuiltin(vmv_v_x_i32m4, "q8ZiZi", "", )

RISCVBuiltin(vmv_v_v_i32m8, "q16Ziq16Zi", "", )

RISCVBuiltin(vmv_v_x_i32m8, "q16ZiZi", "", )

RISCVBuiltin(vmv_v_v_i64m1, "q1Wiq1Wi", "", )

RISCVBuiltin(vmv_v_x_i64m1, "q1WiWi", "", )

RISCVBuiltin(vmv_v_v_i64m2, "q2Wiq2Wi", "", )

RISCVBuiltin(vmv_v_x_i64m2, "q2WiWi", "", )

RISCVBuiltin(vmv_v_v_i64m4, "q4Wiq4Wi", "", )

RISCVBuiltin(vmv_v_x_i64m4, "q4WiWi", "", )

RISCVBuiltin(vmv_v_v_i64m8, "q8Wiq8Wi", "", )

RISCVBuiltin(vmv_v_x_i64m8, "q8WiWi", "", )

RISCVBuiltin(vmv_v_v_u8m1, "q8Ucq8Uc", "", )

RISCVBuiltin(vmv_v_x_u8m1, "q8UcUc", "", )

RISCVBuiltin(vmv_v_v_u8m2, "q16Ucq16Uc", "", )

RISCVBuiltin(vmv_v_x_u8m2, "q16UcUc", "", )

RISCVBuiltin(vmv_v_v_u8m4, "q32Ucq32Uc", "", )

RISCVBuiltin(vmv_v_x_u8m4, "q32UcUc", "", )

RISCVBuiltin(vmv_v_v_u8m8, "q64Ucq64Uc", "", )

RISCVBuiltin(vmv_v_x_u8m8, "q64UcUc", "", )

RISCVBuiltin(vmv_v_v_u16m1, "q4Usq4Us", "", )

RISCVBuiltin(vmv_v_x_u16m1, "q4UsUs", "", )

RISCVBuiltin(vmv_v_v_u16m2, "q8Usq8Us", "", )

RISCVBuiltin(vmv_v_x_u16m2, "q8UsUs", "", )

RISCVBuiltin(vmv_v_v_u16m4, "q16Usq16Us", "", )

RISCVBuiltin(vmv_v_x_u16m4, "q16UsUs", "", )

RISCVBuiltin(vmv_v_v_u16m8, "q32Usq32Us", "", )

RISCVBuiltin(vmv_v_x_u16m8, "q32UsUs", "", )

RISCVBuiltin(vmv_v_v_u32m1, "q2UZiq2UZi", "", )

RISCVBuiltin(vmv_v_x_u32m1, "q2UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u32m2, "q4UZiq4UZi", "", )

RISCVBuiltin(vmv_v_x_u32m2, "q4UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u32m4, "q8UZiq8UZi", "", )

RISCVBuiltin(vmv_v_x_u32m4, "q8UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u32m8, "q16UZiq16UZi", "", )

RISCVBuiltin(vmv_v_x_u32m8, "q16UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u64m1, "q1UWiq1UWi", "", )

RISCVBuiltin(vmv_v_x_u64m1, "q1UWiUWi", "", )

RISCVBuiltin(vmv_v_v_u64m2, "q2UWiq2UWi", "", )

RISCVBuiltin(vmv_v_x_u64m2, "q2UWiUWi", "", )

RISCVBuiltin(vmv_v_v_u64m4, "q4UWiq4UWi", "", )

RISCVBuiltin(vmv_v_x_u64m4, "q4UWiUWi", "", )

RISCVBuiltin(vmv_v_v_u64m8, "q8UWiq8UWi", "", )

RISCVBuiltin(vmv_v_x_u64m8, "q8UWiUWi", "", )

RISCVBuiltin(vmv_v_v_f32m1, "q2fq2f", "", )

RISCVBuiltin(vfmv_v_f_f32m1, "q2ff", "", )

RISCVBuiltin(vmv_v_v_f32m2, "q4fq4f", "", )

RISCVBuiltin(vfmv_v_f_f32m2, "q4ff", "", )

RISCVBuiltin(vmv_v_v_f32m4, "q8fq8f", "", )

RISCVBuiltin(vfmv_v_f_f32m4, "q8ff", "", )

RISCVBuiltin(vmv_v_v_f32m8, "q16fq16f", "", )

RISCVBuiltin(vfmv_v_f_f32m8, "q16ff", "", )

RISCVBuiltin(vmv_v_v_f64m1, "q1dq1d", "", )

RISCVBuiltin(vfmv_v_f_f64m1, "q1dd", "", )

RISCVBuiltin(vmv_v_v_f64m2, "q2dq2d", "", )

RISCVBuiltin(vfmv_v_f_f64m2, "q2dd", "", )

RISCVBuiltin(vmv_v_v_f64m4, "q4dq4d", "", )

RISCVBuiltin(vfmv_v_f_f64m4, "q4dd", "", )

RISCVBuiltin(vmv_v_v_f64m8, "q8dq8d", "", )

RISCVBuiltin(vfmv_v_f_f64m8, "q8dd", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m1, "q8Scq8Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m1, "q8Ucq8Sc", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m2, "q16Scq16Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m2, "q16Ucq16Sc", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m4, "q32Scq32Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m4, "q32Ucq32Sc", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m8, "q64Scq64Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m8, "q64Ucq64Sc", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m1, "q4Ssq4Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m1, "q4Usq4Ss", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m2, "q8Ssq8Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m2, "q8Usq8Ss", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m4, "q16Ssq16Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m4, "q16Usq16Ss", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m8, "q32Ssq32Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m8, "q32Usq32Ss", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m1, "q2UZiq2f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m1, "q2fq2UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m1, "q2Ziq2f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m1, "q2fq2Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m1, "q2Ziq2UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m1, "q2UZiq2Zi", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m2, "q4UZiq4f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m2, "q4fq4UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m2, "q4Ziq4f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m2, "q4fq4Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m2, "q4Ziq4UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m2, "q4UZiq4Zi", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m4, "q8UZiq8f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m4, "q8fq8UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m4, "q8Ziq8f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m4, "q8fq8Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m4, "q8Ziq8UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m4, "q8UZiq8Zi", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m8, "q16UZiq16f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m8, "q16fq16UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m8, "q16Ziq16f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m8, "q16fq16Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m8, "q16Ziq16UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m8, "q16UZiq16Zi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m1, "q1UWiq1d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m1, "q1dq1UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m1, "q1Wiq1d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m1, "q1dq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m1, "q1Wiq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m1, "q1UWiq1Wi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m2, "q2UWiq2d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m2, "q2dq2UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m2, "q2Wiq2d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m2, "q2dq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m2, "q2Wiq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m2, "q2UWiq2Wi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m4, "q4UWiq4d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m4, "q4dq4UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m4, "q4Wiq4d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m4, "q4dq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m4, "q4Wiq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m4, "q4UWiq4Wi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m8, "q8UWiq8d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m8, "q8dq8UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m8, "q8Wiq8d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m8, "q8dq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m8, "q8Wiq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m8, "q8UWiq8Wi", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m1, "q2Ziq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m1, "q1Wiq2Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m1, "q4Ssq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m1, "q1Wiq4Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m1, "q8Scq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m1, "q1Wiq8Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m1, "q4Ssq2Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m1, "q2Ziq4Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m1, "q8Scq2Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m1, "q2Ziq8Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m1, "q8Scq4Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m1, "q4Ssq8Sc", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m2, "q4Ziq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m2, "q2Wiq4Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m2, "q8Ssq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m2, "q2Wiq8Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m2, "q16Scq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m2, "q2Wiq16Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m2, "q8Ssq4Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m2, "q4Ziq8Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m2, "q16Scq4Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m2, "q4Ziq16Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m2, "q16Scq8Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m2, "q8Ssq16Sc", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m4, "q8Ziq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m4, "q4Wiq8Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m4, "q16Ssq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m4, "q4Wiq16Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m4, "q32Scq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m4, "q4Wiq32Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m4, "q16Ssq8Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m4, "q8Ziq16Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m4, "q32Scq8Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m4, "q8Ziq32Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m4, "q32Scq16Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m4, "q16Ssq32Sc", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m8, "q16Ziq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m8, "q8Wiq16Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m8, "q32Ssq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m8, "q8Wiq32Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m8, "q64Scq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m8, "q8Wiq64Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m8, "q32Ssq16Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m8, "q16Ziq32Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m8, "q64Scq16Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m8, "q16Ziq64Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m8, "q64Scq32Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m8, "q32Ssq64Sc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m1, "q2UZiq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m1, "q1UWiq2UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m1, "q4Usq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m1, "q1UWiq4Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m1, "q8Ucq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m1, "q1UWiq8Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m1, "q4Usq2UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m1, "q2UZiq4Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m1, "q8Ucq2UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m1, "q2UZiq8Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m1, "q8Ucq4Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m1, "q4Usq8Uc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m2, "q4UZiq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m2, "q2UWiq4UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m2, "q8Usq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m2, "q2UWiq8Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m2, "q16Ucq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m2, "q2UWiq16Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m2, "q8Usq4UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m2, "q4UZiq8Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m2, "q16Ucq4UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m2, "q4UZiq16Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m2, "q16Ucq8Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m2, "q8Usq16Uc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m4, "q8UZiq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m4, "q4UWiq8UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m4, "q16Usq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m4, "q4UWiq16Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m4, "q32Ucq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m4, "q4UWiq32Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m4, "q16Usq8UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m4, "q8UZiq16Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m4, "q32Ucq8UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m4, "q8UZiq32Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m4, "q32Ucq16Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m4, "q16Usq32Uc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m8, "q16UZiq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m8, "q8UWiq16UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m8, "q32Usq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m8, "q8UWiq32Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m8, "q64Ucq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m8, "q8UWiq64Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m8, "q32Usq16UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m8, "q16UZiq32Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m8, "q64Ucq16UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m8, "q16UZiq64Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m8, "q64Ucq32Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m8, "q32Usq64Uc", "", )

RISCVBuiltin(vslideup_vx_i8m1, "q8Scq8Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i8m2, "q16Scq16Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i8m4, "q32Scq32Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i8m8, "q64Scq64Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m1, "q4Ssq4Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m2, "q8Ssq8Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m4, "q16Ssq16Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m8, "q32Ssq32Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m1, "q2Ziq2Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m2, "q4Ziq4Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m4, "q8Ziq8Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m8, "q16Ziq16Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m1, "q1Wiq1Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m2, "q2Wiq2Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m4, "q4Wiq4Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m8, "q8Wiq8Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m1, "q8Ucq8Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m2, "q16Ucq16Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m4, "q32Ucq32Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m8, "q64Ucq64Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m1, "q4Usq4Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m2, "q8Usq8Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m4, "q16Usq16Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m8, "q32Usq32Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m1, "q2UZiq2UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m2, "q4UZiq4UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m4, "q8UZiq8UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m8, "q16UZiq16UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m1, "q1UWiq1UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m2, "q2UWiq2UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m4, "q4UWiq4UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m8, "q8UWiq8UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m1, "q2fq2fz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m2, "q4fq4fz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m4, "q8fq8fz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m8, "q16fq16fz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m1, "q1dq1dz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m2, "q2dq2dz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m4, "q4dq4dz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m8, "q8dq8dz", "", 2, )

RISCVBuiltin(vmset_m_b1, "q1b", "", )

RISCVBuiltin(vmset_m_b2, "q2b", "", )

RISCVBuiltin(vmset_m_b4, "q4b", "", )

RISCVBuiltin(vmset_m_b8, "q8b", "", )

RISCVBuiltin(vmset_m_b16, "q16b", "", )

RISCVBuiltin(vmset_m_b32, "q32b", "", )

RISCVBuiltin(vmset_m_b64, "q64b", "", )

RISCVBuiltin(vmclr_m_b1, "q1b", "", )

RISCVBuiltin(vmclr_m_b2, "q2b", "", )

RISCVBuiltin(vmclr_m_b4, "q4b", "", )

RISCVBuiltin(vmclr_m_b8, "q8b", "", )

RISCVBuiltin(vmclr_m_b16, "q16b", "", )

RISCVBuiltin(vmclr_m_b32, "q32b", "", )

RISCVBuiltin(vmclr_m_b64, "q64b", "", )

// RISCVBuiltin(vadd_vv_i8m1, "q8Scq8ScqSc", "", )

// RISCVBuiltin(vadd_vv_i16m1, "q1iq1iq1i", "", )

RISCVBuiltin(vadd_vv_i32m1, "q1iq1iq1i", "", )

RISCVBuiltin(vadd_vv_i64m1, "q1iq1iq1i", "", )

RISCVBuiltin(vadd_vv_u8m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vadd_vv_u16m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vadd_vv_u32m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vadd_vv_u64m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vadd_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vadd_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vadd_vv_i8m2, "q2iq2iq2i", "", )

RISCVBuiltin(vadd_vv_i16m2, "q2iq2iq2i", "", )

RISCVBuiltin(vadd_vv_i32m2, "q2iq2iq2i", "", )

RISCVBuiltin(vadd_vv_i64m2, "q2iq2iq2i", "", )

RISCVBuiltin(vadd_vv_u8m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vadd_vv_u16m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vadd_vv_u32m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vadd_vv_u64m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vadd_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vadd_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vadd_vv_i8m4, "q4iq4iq4i", "", )

RISCVBuiltin(vadd_vv_i16m4, "q4iq4iq4i", "", )

RISCVBuiltin(vadd_vv_i32m4, "q4iq4iq4i", "", )

RISCVBuiltin(vadd_vv_i64m4, "q4iq4iq4i", "", )

RISCVBuiltin(vadd_vv_u8m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vadd_vv_u16m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vadd_vv_u32m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vadd_vv_u64m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vadd_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vadd_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vadd_vv_i8m8, "q8iq8iq8i", "", )

RISCVBuiltin(vadd_vv_i16m8, "q8iq8iq8i", "", )

RISCVBuiltin(vadd_vv_i32m8, "q8iq8iq8i", "", )

RISCVBuiltin(vadd_vv_i64m8, "q8iq8iq8i", "", )

RISCVBuiltin(vadd_vv_u8m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vadd_vv_u16m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vadd_vv_u32m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vadd_vv_u64m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vadd_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vadd_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vsub_vv_i8m1, "q1iq1iq1i", "", )

RISCVBuiltin(vsub_vv_i16m1, "q1iq1iq1i", "", )

RISCVBuiltin(vsub_vv_i32m1, "q1iq1iq1i", "", )

RISCVBuiltin(vsub_vv_i64m1, "q1iq1iq1i", "", )

RISCVBuiltin(vsub_vv_u8m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vsub_vv_u16m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vsub_vv_u32m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vsub_vv_u64m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vsub_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vsub_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vsub_vv_i8m2, "q2iq2iq2i", "", )

RISCVBuiltin(vsub_vv_i16m2, "q2iq2iq2i", "", )

RISCVBuiltin(vsub_vv_i32m2, "q2iq2iq2i", "", )

RISCVBuiltin(vsub_vv_i64m2, "q2iq2iq2i", "", )

RISCVBuiltin(vsub_vv_u8m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vsub_vv_u16m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vsub_vv_u32m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vsub_vv_u64m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vsub_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vsub_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vsub_vv_i8m4, "q4iq4iq4i", "", )

RISCVBuiltin(vsub_vv_i16m4, "q4iq4iq4i", "", )

RISCVBuiltin(vsub_vv_i32m4, "q4iq4iq4i", "", )

RISCVBuiltin(vsub_vv_i64m4, "q4iq4iq4i", "", )

RISCVBuiltin(vsub_vv_u8m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vsub_vv_u16m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vsub_vv_u32m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vsub_vv_u64m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vsub_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vsub_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vsub_vv_i8m8, "q8iq8iq8i", "", )

RISCVBuiltin(vsub_vv_i16m8, "q8iq8iq8i", "", )

RISCVBuiltin(vsub_vv_i32m8, "q8iq8iq8i", "", )

RISCVBuiltin(vsub_vv_i64m8, "q8iq8iq8i", "", )

RISCVBuiltin(vsub_vv_u8m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vsub_vv_u16m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vsub_vv_u32m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vsub_vv_u64m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vsub_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vsub_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vmul_vv_i8m1, "q1iq1iq1i", "", )

RISCVBuiltin(vmul_vv_i16m1, "q1iq1iq1i", "", )

RISCVBuiltin(vmul_vv_i32m1, "q1iq1iq1i", "", )

RISCVBuiltin(vmul_vv_i64m1, "q1iq1iq1i", "", )

RISCVBuiltin(vmul_vv_u8m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vmul_vv_u16m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vmul_vv_u32m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vmul_vv_u64m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vmul_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vmul_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vmul_vv_i8m2, "q2iq2iq2i", "", )

RISCVBuiltin(vmul_vv_i16m2, "q2iq2iq2i", "", )

RISCVBuiltin(vmul_vv_i32m2, "q2iq2iq2i", "", )

RISCVBuiltin(vmul_vv_i64m2, "q2iq2iq2i", "", )

RISCVBuiltin(vmul_vv_u8m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vmul_vv_u16m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vmul_vv_u32m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vmul_vv_u64m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vmul_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vmul_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vmul_vv_i8m4, "q4iq4iq4i", "", )

RISCVBuiltin(vmul_vv_i16m4, "q4iq4iq4i", "", )

RISCVBuiltin(vmul_vv_i32m4, "q4iq4iq4i", "", )

RISCVBuiltin(vmul_vv_i64m4, "q4iq4iq4i", "", )

RISCVBuiltin(vmul_vv_u8m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vmul_vv_u16m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vmul_vv_u32m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vmul_vv_u64m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vmul_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vmul_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vmul_vv_i8m8, "q8iq8iq8i", "", )

RISCVBuiltin(vmul_vv_i16m8, "q8iq8iq8i", "", )

RISCVBuiltin(vmul_vv_i32m8, "q8iq8iq8i", "", )

RISCVBuiltin(vmul_vv_i64m8, "q8iq8iq8i", "", )

RISCVBuiltin(vmul_vv_u8m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vmul_vv_u16m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vmul_vv_u32m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vmul_vv_u64m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vmul_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vmul_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vdiv_vv_i8m1, "q1iq1iq1i", "", )

RISCVBuiltin(vdiv_vv_i16m1, "q1iq1iq1i", "", )

RISCVBuiltin(vdiv_vv_i32m1, "q1iq1iq1i", "", )

RISCVBuiltin(vdiv_vv_i64m1, "q1iq1iq1i", "", )

RISCVBuiltin(vdiv_vv_u8m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vdiv_vv_u16m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vdiv_vv_u32m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vdiv_vv_u64m1, "q1Uiq1Uiq1Ui", "", )

RISCVBuiltin(vdiv_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vdiv_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vdiv_vv_i8m2, "q2iq2iq2i", "", )

RISCVBuiltin(vdiv_vv_i16m2, "q2iq2iq2i", "", )

RISCVBuiltin(vdiv_vv_i32m2, "q2iq2iq2i", "", )

RISCVBuiltin(vdiv_vv_i64m2, "q2iq2iq2i", "", )

RISCVBuiltin(vdiv_vv_u8m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vdiv_vv_u16m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vdiv_vv_u32m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vdiv_vv_u64m2, "q2Uiq2Uiq2Ui", "", )

RISCVBuiltin(vdiv_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vdiv_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vdiv_vv_i8m4, "q4iq4iq4i", "", )

RISCVBuiltin(vdiv_vv_i16m4, "q4iq4iq4i", "", )

RISCVBuiltin(vdiv_vv_i32m4, "q4iq4iq4i", "", )

RISCVBuiltin(vdiv_vv_i64m4, "q4iq4iq4i", "", )

RISCVBuiltin(vdiv_vv_u8m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vdiv_vv_u16m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vdiv_vv_u32m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vdiv_vv_u64m4, "q4Uiq4Uiq4Ui", "", )

RISCVBuiltin(vdiv_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vdiv_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vdiv_vv_i8m8, "q8iq8iq8i", "", )

RISCVBuiltin(vdiv_vv_i16m8, "q8iq8iq8i", "", )

RISCVBuiltin(vdiv_vv_i32m8, "q8iq8iq8i", "", )

RISCVBuiltin(vdiv_vv_i64m8, "q8iq8iq8i", "", )

RISCVBuiltin(vdiv_vv_u8m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vdiv_vv_u16m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vdiv_vv_u32m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vdiv_vv_u64m8, "q8Uiq8Uiq8Ui", "", )

RISCVBuiltin(vdiv_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vdiv_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfadd_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfadd_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfadd_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfadd_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfadd_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfadd_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfadd_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfadd_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfsub_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfsub_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfsub_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfsub_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfsub_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfsub_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfsub_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfsub_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfmul_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfmul_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfmul_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfmul_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfmul_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfmul_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfmul_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfmul_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfdiv_vv_f32m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfdiv_vv_f64m1, "q1fq1fq1f", "", )

RISCVBuiltin(vfdiv_vv_f32m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfdiv_vv_f64m2, "q2fq2fq2f", "", )

RISCVBuiltin(vfdiv_vv_f32m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfdiv_vv_f64m4, "q4fq4fq4f", "", )

RISCVBuiltin(vfdiv_vv_f32m8, "q8fq8fq8f", "", )

RISCVBuiltin(vfdiv_vv_f64m8, "q8fq8fq8f", "", )

RISCVBuiltin(vwadd_vv_i16m2, "q4Ziq4Ziq4Zi", "", )

RISCVBuiltin(vwaddu_vv_u16m2, "q4UZiq4UZiq4UZi", "", )

RISCVBuiltin(vwadd_vv_i16m4, "q4Ziq4Ziq4Zi", "", )

RISCVBuiltin(vwaddu_vv_u16m4, "q4UZiq4UZiq4UZi", "", )

RISCVBuiltin(vwadd_vv_i16m8, "q4Ziq4Ziq4Zi", "", )

RISCVBuiltin(vwaddu_vv_u16m8, "q4UZiq4UZiq4UZi", "", )

RISCVBuiltin(vwadd_vv_i32m2, "q2Ziq2Ziq2Zi", "", )

RISCVBuiltin(vwaddu_vv_u32m2, "q2UZiq2UZiq2UZi", "", )

RISCVBuiltin(vwadd_vv_i32m4, "q2Ziq2Ziq2Zi", "", )

RISCVBuiltin(vwaddu_vv_u32m4, "q2UZiq2UZiq2UZi", "", )

RISCVBuiltin(vwadd_vv_i32m8, "q2Ziq2Ziq2Zi", "", )

RISCVBuiltin(vwaddu_vv_u32m8, "q2UZiq2UZiq2UZi", "", )

RISCVBuiltin(vwadd_vv_i64m2, "q1Ziq1Ziq1Zi", "", )

RISCVBuiltin(vwaddu_vv_u64m2, "q1UZiq1UZiq1UZi", "", )

RISCVBuiltin(vwadd_vv_i64m4, "q1Ziq1Ziq1Zi", "", )

RISCVBuiltin(vwaddu_vv_u64m4, "q1UZiq1UZiq1UZi", "", )

RISCVBuiltin(vwadd_vv_i64m8, "q1Ziq1Ziq1Zi", "", )

RISCVBuiltin(vwaddu_vv_u64m8, "q1UZiq1UZiq1UZi", "", )

RISCVBuiltin(vwadd_wv_i16m2, "q4Ziq4Ziq8Zi", "", )

RISCVBuiltin(vwaddu_wv_u16m2, "q4UZiq4UZiq8UZi", "", )

RISCVBuiltin(vwadd_wv_i16m4, "q4Ziq4Ziq8Zi", "", )

RISCVBuiltin(vwaddu_wv_u16m4, "q4UZiq4UZiq8UZi", "", )

RISCVBuiltin(vwadd_wv_i16m8, "q4Ziq4Ziq8Zi", "", )

RISCVBuiltin(vwaddu_wv_u16m8, "q4UZiq4UZiq8UZi", "", )

RISCVBuiltin(vwadd_wv_i32m2, "q2Ziq2Ziq4Zi", "", )

RISCVBuiltin(vwaddu_wv_u32m2, "q2UZiq2UZiq4UZi", "", )

RISCVBuiltin(vwadd_wv_i32m4, "q2Ziq2Ziq4Zi", "", )

RISCVBuiltin(vwaddu_wv_u32m4, "q2UZiq2UZiq4UZi", "", )

RISCVBuiltin(vwadd_wv_i32m8, "q2Ziq2Ziq4Zi", "", )

RISCVBuiltin(vwaddu_wv_u32m8, "q2UZiq2UZiq4UZi", "", )

RISCVBuiltin(vwadd_wv_i64m2, "q1Ziq1Ziq2Zi", "", )

RISCVBuiltin(vwaddu_wv_u64m2, "q1UZiq1UZiq2UZi", "", )

RISCVBuiltin(vwadd_wv_i64m4, "q1Ziq1Ziq2Zi", "", )

RISCVBuiltin(vwaddu_wv_u64m4, "q1UZiq1UZiq2UZi", "", )

RISCVBuiltin(vwadd_wv_i64m8, "q1Ziq1Ziq2Zi", "", )

RISCVBuiltin(vwaddu_wv_u64m8, "q1UZiq1UZiq2UZi", "", )

RISCVBuiltin(vwsub_vv_i16m2, "q4Ziq4Ziq4Zi", "", )

RISCVBuiltin(vwsubu_vv_u16m2, "q4UZiq4UZiq4UZi", "", )

RISCVBuiltin(vwsub_vv_i16m4, "q4Ziq4Ziq4Zi", "", )

RISCVBuiltin(vwsubu_vv_u16m4, "q4UZiq4UZiq4UZi", "", )

RISCVBuiltin(vwsub_vv_i16m8, "q4Ziq4Ziq4Zi", "", )

RISCVBuiltin(vwsubu_vv_u16m8, "q4UZiq4UZiq4UZi", "", )

RISCVBuiltin(vwsub_vv_i32m2, "q2Ziq2Ziq2Zi", "", )

RISCVBuiltin(vwsubu_vv_u32m2, "q2UZiq2UZiq2UZi", "", )

RISCVBuiltin(vwsub_vv_i32m4, "q2Ziq2Ziq2Zi", "", )

RISCVBuiltin(vwsubu_vv_u32m4, "q2UZiq2UZiq2UZi", "", )

RISCVBuiltin(vwsub_vv_i32m8, "q2Ziq2Ziq2Zi", "", )

RISCVBuiltin(vwsubu_vv_u32m8, "q2UZiq2UZiq2UZi", "", )

RISCVBuiltin(vwsub_vv_i64m2, "q1Ziq1Ziq1Zi", "", )

RISCVBuiltin(vwsubu_vv_u64m2, "q1UZiq1UZiq1UZi", "", )

RISCVBuiltin(vwsub_vv_i64m4, "q1Ziq1Ziq1Zi", "", )

RISCVBuiltin(vwsubu_vv_u64m4, "q1UZiq1UZiq1UZi", "", )

RISCVBuiltin(vwsub_vv_i64m8, "q1Ziq1Ziq1Zi", "", )

RISCVBuiltin(vwsubu_vv_u64m8, "q1UZiq1UZiq1UZi", "", )

RISCVBuiltin(vwsub_wv_i16m2, "q4Ziq4Ziq8Zi", "", )

RISCVBuiltin(vwsubu_wv_u16m2, "q4UZiq4UZiq8UZi", "", )

RISCVBuiltin(vwsub_wv_i16m4, "q4Ziq4Ziq8Zi", "", )

RISCVBuiltin(vwsubu_wv_u16m4, "q4UZiq4UZiq8UZi", "", )

RISCVBuiltin(vwsub_wv_i16m8, "q4Ziq4Ziq8Zi", "", )

RISCVBuiltin(vwsubu_wv_u16m8, "q4UZiq4UZiq8UZi", "", )

RISCVBuiltin(vwsub_wv_i32m2, "q2Ziq2Ziq4Zi", "", )

RISCVBuiltin(vwsubu_wv_u32m2, "q2UZiq2UZiq4UZi", "", )

RISCVBuiltin(vwsub_wv_i32m4, "q2Ziq2Ziq4Zi", "", )

RISCVBuiltin(vwsubu_wv_u32m4, "q2UZiq2UZiq4UZi", "", )

RISCVBuiltin(vwsub_wv_i32m8, "q2Ziq2Ziq4Zi", "", )

RISCVBuiltin(vwsubu_wv_u32m8, "q2UZiq2UZiq4UZi", "", )

RISCVBuiltin(vwsub_wv_i64m2, "q1Ziq1Ziq2Zi", "", )

RISCVBuiltin(vwsubu_wv_u64m2, "q1UZiq1UZiq2UZi", "", )

RISCVBuiltin(vwsub_wv_i64m4, "q1Ziq1Ziq2Zi", "", )

RISCVBuiltin(vwsubu_wv_u64m4, "q1UZiq1UZiq2UZi", "", )

RISCVBuiltin(vwsub_wv_i64m8, "q1Ziq1Ziq2Zi", "", )

RISCVBuiltin(vwsubu_wv_u64m8, "q1UZiq1UZiq2UZi", "", )

#undef RISCVBuiltin
