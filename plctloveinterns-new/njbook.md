## 南京分部图书采购每月（5日）一次，图书购买请于每月5日之前联系 @chenjiawei 

## PLCT 南京图书维护表(6月 12日)

| 图书名称 | 图书数量 | 借阅人 |
| ------ | ------ | ------ |
| 程序员的自我修养：链接、装载与库 | 3 | lyh |
| JavaScript语言精粹（修订版） | 4 | qj cjw cy lyh |
| 代码整洁之道 | 4 | qj lyh cjw |
| 程序设计实践 英文版 | 3 |cjw cy lyh |
| 深入理解计算机系统（原书第3版）| 2 | cy |
| PyTorch深度学习 | 2 | cjw cy |
| 两周自制脚本语言 | 5 | cjw cy lyh |
| 自制编译器 | 3 | lyh |
| 现代编译原理 C语言描述 修订版 | 5 | gzy |
| 编译器设计 第2版 | 5 | gzy lyh |
| 计算机组成与设计：硬件/软件接口（英文版·原书第5版·RISC-V版）| 2 | cjw cy |
| LLVM编译器实战教程| 3 |cy lyh cjw |
| 深入理解Java虚拟机：JVM高级特性与最佳实践（第3版）| 4 | cjw cy lyh gzy|
| CAX工程应用丛书：ADAMS 2018虚拟样机技术从入门到精通 | 1 | xzx |
| 材料力学2（第6版） | 1 | xzx |
| 图解工业机器人控制与PLC通信 | 1 | xzx |
| “中国制造2025”出版工程--工业机器人集成系统与模块化 | 1 | xzx |
| “中国制造2025”出版工程--工业机器人系统设计 | 1 | xzx |
| Effective C++：改善程序与设计的55个具体做法  | 1 | nj |
| C++函数式编程 | 1 | lyh |
| C++性能优化指南 | 1 | nj |
| C和C++经典著作：C和指针 POINTERS ON C | 1 | nj |
| C++并发编程实战 | 1 | nj |
| C++编程思想（两卷合订本） | 1 | nj |
| C++ Templates 第2版 英文版 | 1 | nj |
| C++17入门经典（第5版） | 1 | nj |
| Essential C++中文版 | 1 | nj |
| More Effective C++：35个改善编程与设计的有效方法 | 1 | nj |
| C Primer Plus 第6版 中文版 | 1 | nj |
| Linux C与C++ 一线开发实践 | 1 | cjw |
| C Primer Plus 第6版 中文版习题解答 | 1 | nj |
| 计算机视觉：算法与应用 | 1 | nj |
| 清华计算机图书译丛：计算机视觉基础 | 1 | nj |
| 图像处理中的数学修炼（第2版） | 1 | nj |
| 计算机图形学编程 使用OpenGL和C++ | 1 | nj |
| 图像重建（英文版） | 1 | nj |
| 数字图像处理（第4版） | 1 | nj |
| 数字图像处理（第四版） | 1 | nj |
| 视觉计算基础：计算机视觉、图形学和图像处理的核心概念  | 1 | nj |
| 计算机视觉：模型、学习和推理/计算机科学丛书 | 1 | nj |
| 深度学习与计算机视觉：算法原理、框架应用与代码实现 | 1 | nj |
| 计算机图形学（第4版） | 1 | nj |
| 计算机视觉――一种现代方法（第二版）（英文版） | 1 | nj |
| Python计算机视觉编程 | 1 | nj |
| 图像处理、分析与机器视觉·第4版 | 1 | nj |




总计： 77 本 外借： qj 2本 gzy 3本 xzx 5本

## PLCT 7月预采购数目：

计算机组成设计 RISCV 版 

(待补充)