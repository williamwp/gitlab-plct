# RVGF2020 目标：所有RV相关项目都去RISC-V全球论坛报告一次

## Submit Plan

* [ ] rvv-llvm @xmj @zhangyin @wangpeng_iie @zhangzhang @luxufan
* [ ] V8 RISC-V Porting @qjivy @zouxiaofang @luyahan @chenjiayou @yangwenzhang
* [ ] OpenArkCompiler RISC-V Porting (w/o futurewei) @shining
* [ ] QEMU support for Neclei, RVV, RT-Thread, etc. @pandora @gaozhiyuan
* [ ] Eternal Balance Project. @w @chenying
* [ ] Spike Snapshot Support. @wangmeng @liweiwei


## CFP

https://events.linuxfoundation.org/riscv-global-forum/program/cfp/

### Overview
The RISC-V Global Forum Call for Proposals is now open.

RISC-V is breaking down technical barriers and disrupting traditional microprocessor business models through global collaboration. The RISC-V Global Forum is our opportunity to engage across the community, from start-ups to multi-nationals, from students to luminaries, from deep technical talks to understanding industry momentum. Join us as a sponsor to showcase success and opportunity, as a speaker to share progress and perspective, and as an attendee to hear from industry thought leaders, engage with your peers, and join the community.

Dates to Remember
* CFP Opens: Thursday, June 11 at 12:00 pm PST
* CFP Closes: Friday, July 10 at 11:59 PM PST
* CFP Notifications: Tuesday, July 21
* Schedule Announcement: Wednesday, July 22
* Presentation Slide Due Date: To Be Announced
* Event Dates: Thursday, September 3, 2020

Tracks and Suggested Topics

Industry
* Embedded
* Internet of Things
* AI / Machine Learning
* HPC
* Cloud
* Automotive
* Venture Capital / Start-up
* Growth and strategy
* Geo specific
* Government
* Universities
* RISC-V Community and collaboration

Technical
* ISA and extensions
* Cores and SoCs
* Accelerators
* System architecture
* Security
* Functional Safety
* Design tools
* Compliance and verification
* Software stack
* Developer boards and resources
* Session Types
* Types of Submissions:


* Keynote
* Breakout Sessions
* Panel Discussion
* Lightning Talks
* Important Notes

All speakers are required to adhere to our Code of Conduct. We also highly recommend that speakers take our Inclusive Speaker Orientation Course.
Panel submissions must include the names of all participants in the initial submission to be considered. In addition, The Linux Foundation does not accept submissions with all-male panels in an effort to increase speaker diversity.
Complimentary Passes For Speakers – One complimentary pass for the event will be provided per accepted speaker (and co-speaker, if required). For accepted panel discussions, up to 5 panelists, + 1 moderator will receive a complimentary event pass; additional panelists will be addressed on a case-by-case basis.
Avoid sales or marketing pitches when preparing your proposal; these talks are almost always rejected due to the fact that they take away from the integrity of our events, and are rarely well-received by conference attendees.
All accepted speakers are required to submit their slides by TBA.

Preparing to Submit

Preparing to Submit Your Proposal

While it is not our intention to provide you with strict instructions on how to prepare your proposal, we hope you will take a moment to review the following guidelines that we have put together to help you prepare the best submission possible. To get started, here are three things that you should consider before submitting your proposal:

What are you hoping to get from your presentation?

What do you expect the audience to gain from your presentation?

How will your presentation help better the ecosystem?

There are plenty of ways to give a presentation about projects and technologies without focusing on company-specific efforts. Remember the things to consider that we mentioned above when writing your proposal and think of ways to make it interesting for attendees while still letting you share your experiences, educate the community about an issue, or generate interest in a project.

First Time Submitting? Don’t Feel Intimidated

Linux Foundation events are an excellent way to get to know the community and share your ideas and the work that you are doing and we strongly encourage first-time speakers to submit talks for our events. In the instance that you aren’t sure about your abstract, reach out to us and we will be more than happy to work with you on your proposal.

How To Give a Great Tech Talk

In the instance that your talk is accepted, we want to make sure that you give the best presentation possible. To do this, we enlisted the help of seasoned conference speaker Josh Berkus who has prepared an in-depth tutorial on “How to Give a Great Tech Talk”.

Part 1: http://www.youtube.com/watch?v=iE9y3gyF8Kw

Part 2: http://www.youtube.com/watch?v=gcOP4WQfJl4

Code of Conduct

The Linux Foundation is dedicated to providing a harassment-free experience for participants at all of our events. We encourage all submitters to review our complete Code of Conduct.
