[TOC]

这份文档是PLCT知识库，包括各类资料、调研、学习笔记等。

# 1 BenchCouncil

**成立日期：** 2018年6月份

Member构成： 1期member里面有软件所、中科大，后续有计算所， 其他: 阿里巴巴、百度、腾讯、微软亚洲研究院、寒武纪、RISC-V联盟、58同城、网易、字节跳动、中国计量科学研究院、知乎、联想、Paypal、墨奇、华为、京东、普林斯顿大学、中科云达、云天励飞... 没有比特大陆 大学的成员里面，美国有几所，但都不是rank1的大学

**官网：** http://www.benchcouncil.org/cn/cn.html

**使命：**

国际开放测试基准和标准委员会（BenchCouncil，国际测试委员会）是一个**非营利性**的国际研究机构，旨在促进**开源芯片**、**人工智能**和**大数据**等新技术的评价、验证、研讨、孵化和推广。其使命如下：

(1) 在全球范围内倡导新技术的产业规则。具体包括以 Benchmark( 测试标准）为主的标准制定；**发布性能榜**，引领产业健康发展

(2) 促进新技术（基础和主干的技术创新）的验证和推广：包括创建**实验床 (testbed)**；举办国际竞赛。

## 1.1 AIBench

 **AIBench** 是 BenchCouncil 组织联合中科院计算所、阿里巴巴、百度、腾讯、微软亚洲研究院、寒武纪、RISC-V联盟、58同城、网易、字节跳动、中国计量科学研究院、知乎、联想、Paypal、墨奇、华为、京东、普林斯顿大学、中科云达、云天励飞等数十家研究机构和公司共同推出的首个工业标准的人工智能测试标准，它抽象了以搜索引擎、电子商务和社交网络为代表的互联网服务应用中主要的16种AI问题域，涵盖图像分类、图像生成、文本翻译、看图说话、图像变换、语音识别、人脸识别、三维人脸识别、目标检测、视频预测、图像压缩、智能推荐、三维物体重建、文本摘要生成、空间变换以及智能排序，并针对这些问题域提供了一个高度可扩展、可配置以及灵活的测试标准构建框架，支持快速构建真实的端到端应用级测试标准、组件级测试标准以及微测试标准。目前，AIBench提供12个微测试标准，如卷积、全连接、激活函数等，16个组件级测试标准，以及2个应用级测试标准---电商搜索和DCMix，包含人工智能的在线推理模块和离线训练模块。同时，AIBench包含16种真实数据集，覆盖文本、图像、音频、视频、3D数据，并提供多种软件栈的实现。

分为以下部分：

1. AIBench_Framework: 顾名思义
2. DC_AIBench_Micro：

python写的调用Tensorflow的基本算子，

- No. DC-AI-M1: Convolution,
- No. DC-AI-M2: Fully Connected
- No. DC-AI-M3: Relu
- No. DC-AI-M4: Sigmoid
- No. DC-AI-M5: Tanh
- No. DC-AI-M6: MaxPooling
- No. DC-AI-M7: AvgPooling
- No. DC-AI-M8: CosineNorm
- No. DC-AI-M9: BatchNorm
- No. DC-AI-M10: Dropout
- No. DC-AI-M11: Element-wise multiply
- No. DC-AI-M12: Softmax

3. AIBench_Application_Benchmark :模拟网络环境下， C、S模式的AI应用场景

- online-model：

   [![image](https://yt.droid.ac.cn/tiger/plctloveinterns/uploads/3a597926a3443569ade30115eaabd3c6/image.png)](https://yt.droid.ac.cn/tiger/plctloveinterns/uploads/3a597926a3443569ade30115eaabd3c6/image.png)

- offline-model

   [![image](https://yt.droid.ac.cn/tiger/plctloveinterns/uploads/a58d615344caf6d4a1112682f2b32a4c/image.png)](https://yt.droid.ac.cn/tiger/plctloveinterns/uploads/a58d615344caf6d4a1112682f2b32a4c/image.png)

4. DC_AIBench_Component ：AI领域的数据库benchmark
5. AIBench_DCMIX： AI领域的数据库bm，创建延迟不同的model workload对数据库施加负载

性能榜（only one until now）

1. 09/26/2019： NVIDA GPU训练性能，分为单卡/多卡训练， 单卡推理的测试 **注：国内没有任何一家AI芯片公司送样品去测试，最早有芯片出品的比特大陆，也没有参与这个组织。**

参考：http://www.benchcouncil.org/cn/numbers.html

# 2 sifive unleash+nvdla的相关技术资料

2018年底sifive的talk pdf：

[Nvidias-Deep-Learning-Accelerator-Meets-SiFives-Freedom-Platform-Frans-Sijstermans-and-Yunsup-Lee.pdf]()

– NVDLA

• https://github.com/nvdla/hw

• https://github.com/nvdla/sw

• http://nvdla.org

– Freedom Platform

• https://github.com/sifive/freedom

• https://github.com/sifive/nvidia-dla-blocks



# 3 RISC-V平台的可信执行环境: 蓬莱Encalve的调研

了解开源项目：https://trustkernel.com/news/2019/12/31/Penglai-Open-Source.html 

资料罗列：

1. wiki： https://github.com/Penglai-Enclave/Penglai-Enclave/wiki
2. https://github.com/Penglai-Enclave/Penglai-Enclave/wiki/Penglai-Enclave-User-Documentation
3. https://github.com/Penglai-Enclave/Penglai-Enclave/wiki/Penglai-Enclave-Hardware-Extensions 最有用的文档就是这个 [Proposal_of_sPMP__S-mode_PMP_for_RISC-V.pdf](https://yt.droid.ac.cn/tiger/plctloveinterns/uploads/30192fc3223513e9e2efad1d9470c11e/Proposal_of_sPMP__S-mode_PMP_for_RISC-V.pdf)
4. 软件的实现： https://github.com/Penglai-Enclave/Penglai-Enclave/wiki/sPMP-implementation
5. SDK： https://github.com/Penglai-Enclave/Penglai-Enclave/wiki/Penglai-Enclave-SDK-Implementation
6. 代码下载  https://github.com/Penglai-Enclave/Penglai-Enclave

# 4 GNU.Make 中文扫描版.pdf阅读笔记

相关issue：https://yt.droid.ac.cn/tiger/plctloveinterns/-/issues/84

# 5 “The Missing Semester of Your CS Education” 课程学习笔记

课程链接：https://missing.csail.mit.edu/ 

学习笔记（lecture1-3）：https://yt.droid.ac.cn/tiger/plctloveinterns/-/issues/94