# First Work Day

TODO @w

## RVV

1. 办理入职手续

   1. 扫描身份证、学生证以及建行卡。建行卡只需扫描正面。
   
   2. 打印、填写并扫描“软件所临时用工管理办法.doc”，只需填写个人信息。
   
   3. 将上述扫描件发送给吴伟老师。

2. 关于实习工资
   
   第一个月的实习工资与LV无关，一般在1k到2k之间。第二个月进入工作状态。
   
   1. LV1 1.5k/月;
    
   2. LV2 2k/月;
    
   3. LV3 3k/月;
   
   4. LV4 4k/月。
   
3. 加入PLCT微信群后，修改群名片并添加群成员为好友。实习生我们主要通过微信沟通。

4. 给吴伟老师一个常用邮箱地址，之后你会收到一份配置Gitlab密码的邮件，点击链接配置密码。

    之后吴伟老师会给你分配weloveinterns以及rvv项目的访问权限。

    邢老师和张尹同学是rvv项目的maintainer，只有他们有merge和push到master的权限。

    Gitlab网站上的内容属于保密信息，请勿外泄。

5. 你可以把自己的技术类社交帐号列在 https://yt.droid.ac.cn/tiger/plctloveinterns/-/issues/115  。

6. 周末是休息时间，工作日白天收到消息后要求在一小内作出回复。

7. 所有小组，每两周交一次进展报告，每个月1日和16日发布。
   
    之前的报告可在  https://github.com/isrc-cas/PLCT-WeeklyPLCT 找到 。
    
    连续两周没有任何交付物的实习生会被劝退。

8. 必读内容
   
   1. https://yt.droid.ac.cn/tiger/plctloveinterns 。
   
   2. https://github.com/lazyparser/survivial-manual-for-interns ，特别是rules部分。欢迎PR。

9. 推荐阅读
   
    1. https://github.com/lazyparser/weloveinterns 。
    
    2. https://github.com/lazyparser/minimalist-team-leader 。

10. rvv项目相关信息

     1. 去年11月份的报告 htps://www.bilibili.com/video/BV1PJ41147Ek?p=11 。
    
     2. 在 https://space.bilibili.com/296494084/video 搜索王鹏、陈影、陆旭凡、张尹的名字，可以找到之前的组内报告。
    
     3. 以往报告的slides https://github.com/isrc-cas/PLCT-Open-Reports 。
        
        注意参考PR的形式，以后你的报告，做得好的话，同样流程公开和PR到这个仓库。
        
     4. rvv项目开源仓库 https://github.com/isrc-cas/rvv-llvm 。
    
        我们是在y站（即内部Gitlab）开发、合并，然后同步到GitHub。
    
     5. 开源进展·第一期·2020年05月16日 https://zhuanlan.zhihu.com/p/141463489PLCT 。

## V8

TODO @yangwenzhang
