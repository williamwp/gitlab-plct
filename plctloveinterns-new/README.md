# PLCT实习生的日常工作 —— 今天又是核平的一天🎉

最后更新：20200507

[现役实习生及能力等级](interns.md)

[南京分院报销审批等流程](nanjing.md)

[北京本部报销审批流程等](beijing.md)

[PLCT南京图书申请](njbook.md)

如果你有技术博客，跟实习内容相关，请在 #115 评论，追加自己的信息。

https://yt.droid.ac.cn/tiger/plctloveinterns/-/issues/115

## 技巧和经验

如何在拿到ISCAS邮箱之后修改y站的 commit email：

https://yt.droid.ac.cn/tiger/plctloveinterns/-/issues/110

